This repository contains a framework for healthcare trajectory processing and modeling. It is intended to be used after the preprocessing of [SCALPEL-Extraction](https://github.com/X-DataInitiative/SCALPEL-Extraction) (E. Bacry et al., « SCALPEL3: a scalable open-source library for healthcare claims databases », arXiv:1910.07045 [cs], oct. 2019.
)

# Installation

- git pull this repository: `git pull https://gitlab.com/DREES_code/OSAM/cam_gram.git`

- go into the cloned repository

- activate your preferred conda environment: `conda activate my_env`

- install with pip: `pip install -e .`

# Tutorials

- See how to build target-control cohorts in [studies/template](tutorials/template).

- Healthcare trajectory Visualisation in[studies/template](tutorials/template/)
 
# Models

+ Simple batch LSTM model in pytorch

+ Word2vec by SVD of the PMI matrix : WIP

+ Attention model : TODO 

# Tools

+ Describe a cohort

+ Visualize a patient trajectory

+ 
