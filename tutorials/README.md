# Tutoriels sndstraj

## Objectif du package

Ce dossier contient des modèles de codes afin d'utiliser facilement les codes et fonctions du package python `sndstraj`. Le package essaye de faciliter l'étude du SNDS pour sous l'angle des trajectoire de soins. Il est orienté pour une approche cohorte contrôle/témoin mais de nombreuses fonctions pevuent être utiles pour d'autres types d'études (trajectoire ou non).

Tous les outils développés dans ce package supposent que les données du snds ont été extraites grâce à [SCAPEL-Extraction (Bacry et al. 2019)[^1]](https://github.com/X-DataInitiative/SCALPEL-Extraction) et ont été mises au format dit *événementiel* décrit ci-dessous.

Nous utilisons massivement [spark](https://spark.apache.org/) pour le calcul distribué et la construction des cohortes. Le package peut s'utiliser en black box mais est plus utile à des personnes ayant une petite connaissance de python pour adapter le code à leurs usages.

## Format des données

Les données du SNDS ont une structure transactionnelle adaptée à leur usage principal: c'est une base comptable pour le système de santé français et l'assurance maladie. 
Pour faire des études statistiques ou de modélisation avec ces données, il est nécessaire de les reformater. Nous utilisons un format intéressant pour des études longitudinales obtenu avec [Scalpel](https://github.com/X-DataInitiative/SCALPEL-Extraction).
Cette chaîne de traitement consiste à utiliser des extracteurs d'évènements médicaux puisant dans les différentes tables du SNDS et à rassembler tous ces évènements dans des *tables évènementielles* prêtes à l'étude.

| patientID 	| category     	| groupID          	| value               	| weight 	| start      	| end   	|
|-----------	|--------------	|------------------	|---------------------	|--------	|------------	|-------	|
| 0000001   	| mco_ccam_act 	| eta_num__rsa_num 	| GLQPOO2              	| 0.0    	| 2016-12-01 	| null  	|
| 0000002   	| drugs        	| pfs_id           	| A10AC01            	| 0.0    	| 2016-05-24 	|  null 	|


## Tutoriel sélection de population et construction de séquences

Dans ce tutoriel, divers étapes d'une étude cas/témoin ont été ébauchées. Les données sont fictives et disponibles au format parquet dans le dossier [data/raw_data](data/raw_data).  

- Les étapes [0_select_population -  Selection de la population](0_select_population.ipynb) et [Obis_build_low_granularity - Passage à une granularité plus grande (ex: cip13 -> ATC)](0bis_build_low_granularity.ipynb) sont communes à la quasi totalité des tutoriels car elles consistent à selectionner une population et les événements d'intérêt pour le tutoriel sous le format tables événemenetielles.  

- Les étapes [1 - Sélectionner une cohorte cible](1_select_target_cohort.ipynb) et [2 - Sélectionner une contrôle](2_select_control_cohort.ipynb) présentent des codes pour la sélection de population dans le cadre d'une étude cas-témoins.
 
- L'étape [2bis - Visualisation de patients](2bis_visu_patients.ipynb) montre comment faire de la visualisation interactive de patient.

- L'étape [3 - Construction de séquences (pour des modèles)](3_build_event_sequences.ipynb) décrit comment sauver des séquences de nombres prêtes à nourir un modèle de deep learning ou un modèle séquentiel.

## Tutoriel co-occurrences d'événements

#### [4 - Construction de la matrice de cooccurrence](tuto_cooccurrences/1_Cooccurrence.ipynb)

#### [5 - Construction d'embeddings de concepts médicaux](tuto_cooccurrences/3_word2vec_by_svd.ipynb)

## Tutoriel modèles prédictifs

Voir dans tuto_prediction -> TODO

## [Tutoriel variables latentes](/tutorials/tuto_latent_variables_decomposition)

Ce tuto s'inscrit dans le cadre plus général d'une étude dont le but est de dégager les principales co-morbidités et co-médications dans des populations circonscrites épidémiologiquement.
Pour parvenir à cette fin, nous explorons l'utilisation des modèles à variables cachées (Latent Semantic Analysis, Exploratory Factor Analysis).  

Voir le [readme](/tutorials/tuto_latent_variables_decomposition/README.md) du tuto pour plus de détails.

## Références

[^1]: E. Bacry et al., « SCALPEL3: a scalable open-source library for healthcare claims databases », arXiv:1910.07045 [cs], oct. 2019.
