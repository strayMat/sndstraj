# Décomposition d'une matrice d'observation

## Construction de la donnée d'entrée

Une manière simple de représenter la trajectoire de soins d'un patient dans le SNDS est, pour une période donnée, de compter le nombre d'événements que ce patient a eu. Cette construction est appelé *Bag Of Words* en traitement du langage et dans notre cas correspond plus à un *Bag of Events*. Nous ferons référence à ce format sous le terme **BoW** (hérité du traitement du langage) ou **BoE**.

Pour construire cette représentation à partir des tables événementielles, nous renvoyons au premier turoriel : [1-build_bow](1-build_bow.ipynb).

Ici nous décrivons comment analyser ces matrices BoW.

## Analyses

On considère une matrice patient-événements $`X \in \matbb R^{N, V}`$ où N est le nombre de patients considérés et le V est la taille du vocabulaire considéré.
Chaque terme dans ce vocabulaire correspond à un code médical extrait du SNDS et correspondant à un contact avec le système de soin. Ces codes peuvent être des diagnostics (CIM10), des actes (CCAM), des codes prestations (NGAP/PRS_NAT_REF).

Le but est de trouver les événements du vocabulaire qui arrive le plus souvent ensemble, indiquant des co-morbidités et des co-médications. Les détails méthodologiques sont disponibles dans le [tutoriel 2](2-compute_decompositions.ipynb). Pour cela nous dégageons des *facteurs* de termes qui explique le plus de variances dans les données.

## Visualisations

Afin d'appréhender la pertinence de ces modèles, nous essayons de représenter visuellement les facteurs obtenus pour la population d'intérêt. Les fonctions pour obtenir ces visualisations sont présentés dans le 3ème tutoriel [3-plot_decomposition](3-plot_decomposition.ipynb).

