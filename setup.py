from setuptools import setup

setup(
    name='sndstraj',
    version='0.1',
    description='Running trajectory analysis on the snds event tables',
    url='https://gitlab.com/DREES_code/OSAM/lab/sndstraj',
    author='Matthieu Doutreligne',
    author_email='matt.dout@gmail.com',
    license='MIT',
    packages=['sndstraj'],
    zip_safe=False,
    install_requires=
    [
        'pyspark', 'numpy', 'networkx', 'pandas',
        'matplotlib', 'bokeh', 'scikit-learn', 'scipy', 'plotly', 'factor_analyzer'
    ]
)
