from cohorts.cohort import Cohort, SourceCohort, OperationCohort
import pyspark.sql.functions as func
import pandas as pd
from typing import Tuple

from pyspark_tests import PySparkTest

path2test_data = '../data/sample10.parquet'
path2test_patients = '../data/patients10.parquet'


class Testcohort(PySparkTest):
    def test_execute_computation(self):
        events = self.spark.read.parquet(path2test_data)
        cohort = Cohort(name='test_cohort', events=events, characteristics='empty')
        expected = 10
        cohort.execute_computation()
        if self.debugging:
            print('Result:')
            print(cohort.population_size)
            print('Expected:')
            print(expected)
        assert cohort.population_size == expected

    def build_test_cohorts(self) -> Tuple[SourceCohort, SourceCohort]:
        events = self.spark.read.parquet(path2test_data)
        left_events = events.filter(
            func.col('value') == 'Anti_IgE').filter(
            func.col('patientID').isin(['pID-0', 'pID-1', 'pID-6'])).drop('index')
        left_cohort = SourceCohort(
            name='test_left',
            events=left_events,
            characteristics='Anti_IgE events')
        right_events = events.filter(func.col('value') == 'GLQP009').drop('index')
        right_cohort = SourceCohort(
            name='test_right',
            events=right_events,
            characteristics='GLQP009 events')
        return left_cohort, right_cohort

    def test_union_cohort(self):
        left_cohort, right_cohort = self.build_test_cohorts()

        union_cohort = OperationCohort(
            name='test_union',
            left_parent=left_cohort,
            right_parent=right_cohort,
            operation_type='union',
            characteristics='testing union between Anti_Ige and GLQP009')
        union_cohort.execute_computation()

        expected_df = pd.DataFrame({
            'patientID': ['pID-0', 'pID-0', 'pID-1', 'pID-6', 'pID-6', 'pID-0', 'pID-0', 'pID-5', 'pID-6', 'pID-6', 'pID-7'],
            'category': ['drug', 'drug', 'drug', 'drug', 'drug', 'dcir_act', 'dcir_act', 'dcir_act', 'dcir_act', 'dcir_act', 'dcir_act'],
            'value':  ['Anti_IgE', 'Anti_IgE', 'Anti_IgE', 'Anti_IgE', 'Anti_IgE', 'GLQP009', 'GLQP009', 'GLQP009', 'GLQP009', 'GLQP009', 'GLQP009'],
            'groupID': ['pfsID-1', 'pfsID-26', 'pfsID-59', 'pfsID-26', 'pfsID-7', 'pfsID-5', 'pfsID-51', 'pfsID-8', 'pfsID-1', 'pfsID-90', 'pfsID-5'],
            'weight': [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            'start': ['2017-11-03', '2017-12-05', '2017-01-25', '2017-04-11', '2017-01-10', '2017-08-13', '2017-01-10', '2017-02-13', '2017-02-27', '2017-02-11', '2017-10-11']})

        expected_events = self.spark.createDataFrame(expected_df)
        expected_population_size = 5
        # to have the same order of raws
        result_events = union_cohort.events.sort(
            func.col('category').desc(), 'patientID', 'groupID').drop('end')

        if self.debugging:
            if self.debugging:
                print('Result:')
                print(union_cohort.population_size)
                print(result_events.toPandas())
                print('Expected:')
                print(expected_population_size)
                print(expected_events.toPandas())
        assert (union_cohort.population_size == expected_population_size) & \
               (result_events.toPandas().equals(expected_events.toPandas()))

    def test_intersection_cohort(self):
        left_cohort, right_cohort = self.build_test_cohorts()

        union_cohort = OperationCohort(
            name='test_intersection',
            left_parent=left_cohort,
            right_parent=right_cohort,
            operation_type='intersection',
            characteristics='testing intersection between Anti_Ige and GLQP009')
        union_cohort.execute_computation()

        expected_df = pd.DataFrame({
            'patientID': ['pID-0', 'pID-0', 'pID-6', 'pID-6', 'pID-0', 'pID-0', 'pID-6', 'pID-6'],
            'category': ['drug', 'drug', 'drug', 'drug', 'dcir_act', 'dcir_act', 'dcir_act', 'dcir_act'],
            'value':  ['Anti_IgE', 'Anti_IgE', 'Anti_IgE', 'Anti_IgE', 'GLQP009', 'GLQP009', 'GLQP009', 'GLQP009'],
            'groupID': ['pfsID-1', 'pfsID-26', 'pfsID-26', 'pfsID-7', 'pfsID-5', 'pfsID-51',  'pfsID-1', 'pfsID-90'],
            'weight': [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            'start': ['2017-11-03', '2017-12-05', '2017-04-11', '2017-01-10', '2017-08-13', '2017-01-10', '2017-02-27', '2017-02-11']})

        expected_events = self.spark.createDataFrame(expected_df)
        expected_population_size = 2
        # to have the same order of raws
        result_events = union_cohort.events.sort(
            func.col('category').desc(), 'patientID', 'groupID').drop('end')

        if self.debugging:
            if self.debugging:
                print('Result:')
                print(union_cohort.population_size)
                print(result_events.toPandas())
                print('Expected:')
                print(expected_population_size)
                print(expected_events.toPandas())
        assert (union_cohort.population_size == expected_population_size) & \
               (result_events.toPandas().equals(expected_events.toPandas()))

    def test_exclusion_cohort(self):
        left_cohort, right_cohort = self.build_test_cohorts()

        union_cohort = OperationCohort(
            name='test_exclusion',
            left_parent=left_cohort,
            right_parent=right_cohort,
            operation_type='exclusion',
            characteristics='testing exclusion of GLQP009 from Anti_Ige')
        union_cohort.execute_computation()

        expected_df = pd.DataFrame({
            'patientID': ['pID-1'],
            'category': ['drug'],
            'value':  ['Anti_IgE'],
            'groupID': ['pfsID-59'],
            'weight': [0.0],
            'start': ['2017-01-25']})

        expected_events = self.spark.createDataFrame(expected_df)
        expected_population_size = 1
        # to have the same order of raws
        result_events = union_cohort.events.sort(
            func.col('category').desc(), 'patientID', 'groupID').drop('end')

        if self.debugging:
            if self.debugging:
                print('Result:')
                print(union_cohort.population_size)
                print(result_events.toPandas())
                print('Expected:')
                print(expected_population_size)
                print(expected_events.toPandas())
        assert (union_cohort.population_size == expected_population_size) & \
               (result_events.toPandas().equals(expected_events.toPandas()))
