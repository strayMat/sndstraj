from datetime import datetime

import pandas as pd

#  should be started only once for all tests
# sqlcontext = start_sc(4)
from pyspark_tests import PySparkTest
from cohorts.describeCohort import add_age


class TestDescribeCohort(PySparkTest):
    def test_add_ages(self):
        patients_df = pd.DataFrame({
            "patientID": ['pID-0', 'pID-1'],
            "gender": ['1', '2'],
            "birthDate": [datetime.strptime('1975-08-27', '%Y-%m-%d'),
                          datetime.strptime('1970-08-11', '%Y-%m-%d')],
            "deathDate": ['null', 'null']
        })

        patients = self.spark.createDataFrame(patients_df)
        reference_date = '2019-01-01'
        patients_df_w_ages = add_age(
            patients=patients,
            reference_date=reference_date,
            bucket_size=5)

        expected_df = pd.DataFrame({
            "patientID": ['pID-0', 'pID-1'],
            "gender": ['1', '2'],
            "birthDate": [datetime.strptime('1975-08-27', '%Y-%m-%d'),
                          datetime.strptime('1970-08-11', '%Y-%m-%d')],
            "deathDate": ['null', 'null'],
            "age": [43, 48],
            "age_bucket": [8, 9]
        })

        expected = self.spark.createDataFrame(expected_df)
        if self.debugging:
            print('Result:')
            patients_df_w_ages.show()
            print('Expected:')
            expected.show()
        assert patients_df_w_ages.toPandas().equals(expected.toPandas())
