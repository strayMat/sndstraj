import os

import numpy as np
import pandas as pd

from eventsTransformers.oneEncodings import get_scipy_bow
from pyspark_tests import PySparkTest, events_structure, PATH2TMP

events = pd.DataFrame({
    'patientID': ['p1', 'p1', 'p1', 'p1', 'p3', 'p3', 'p4'],
    'category': ['drug', 'drug', 'mco_main_diagnosis', 'mco_main_diagnosis', 'drug',
                 'mco_main_diagnosis', 'ssr_diagnosis'],
    'value': ['A10AB01', 'A10AB02', 'E11', 'A10AB01', 'A10AB01', 'E11', 'E40'],
    'groupID': ['10000', '10000', '10000', '10000', '10000', '10000', '1000'],
    'weight': [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
    'start': ['2019-01-01', '2019-01-10', '2019-01-11', '2019-03-05', '2019-06-01', '2019-07-01',
              '2019-12-01'],
    'end': ['2019-01-01', '2019-01-10', '2019-01-11', '2019-03-05', '2019-06-01', '2019-07-01',
            '2019-12-01']},
)

events['start'] = pd.to_datetime(events['start'])
events['end'] = pd.to_datetime(events['end'])


class TestOneEncodings(PySparkTest):
    def test_get_scipy_bow(self):
        events_df = self.spark.createDataFrame(events, schema=events_structure)
        events_df.show()
        events_df.printSchema()

        scipy_bow, label2ix, patientIDs = get_scipy_bow(
            events=events_df,
            output_dir=os.path.join(PATH2TMP, 'spark_bow'),
            category_black_list=['ssr_diagnosis'],
            verbose=1
        )

        expected_bow = np.array([
            [2, 1, 1],
            [1, 1, 0]
        ])
        expected_label2ix = {'A10AB01': 0, 'E11': 1, 'A10AB02': 2}
        expected_patientIDs = {'p1': 0, 'p3': 1}

        print(scipy_bow.todense())
        print(expected_bow)
        print(expected_label2ix)
        print(label2ix)
        print(expected_patientIDs)
        print(patientIDs)

        assert (expected_label2ix == label2ix) & \
               (expected_patientIDs == patientIDs) & \
               (expected_bow == scipy_bow.todense()).all()
