import os

import pandas as pd

from eventsTransformers.deltaSequences import delta_from_events
from pyspark_tests import PySparkTest, events_structure, PATH2TMP
from utils.HealthEventsVocab import HealthEventsVocab

events = pd.DataFrame({
    'patientID': ['p1', 'p1', 'p1', 'p1', 'p3', 'p3', 'p4'],
    'category': ['drug', 'drug', 'mco_main_diagnosis', 'mco_main_diagnosis', 'drug',
                 'mco_main_diagnosis', 'drug'],
    'value': ['A10AB01', 'A10AB02', 'E11', 'A10AB01', 'A10AB01', 'E11', 'A10AB01'],
    'groupID': ['10000', '10000', '10000', '10000', '10000', '10000', '1000'],
    'weight': [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
    'start': ['2019-01-01', '2019-01-10', '2019-01-11', '2019-03-05', '2019-06-01', '2019-07-01',
              '2019-12-01'],
    'end': ['2019-01-01', '2019-01-10', '2019-01-11', '2019-03-05', '2019-06-01', '2019-07-01',
            '2019-12-01']},
)

events['start'] = pd.to_datetime(events['start'])
events['end'] = pd.to_datetime(events['end'])

path2output_dir = os.path.join(
    PATH2TMP,
    'preprocessed_codes_sequences/'
)


class TestDeltaSequences(PySparkTest):
    def test_delta_from_events(self):
        events_df = self.spark.createDataFrame(events, schema=events_structure)
        events_df.show()
        events_df.printSchema()
        sequences = delta_from_events(
            events_df=events_df,
            output_dir=path2output_dir,
            output_format='pandas',
            with_deltas=False,
            target_patients=None,
            min_nb_events=2,
            categories_blacklist=None,
            verbose=1).reset_index()

        expected__sequences = pd.DataFrame({
            'patientID': ['p1', 'p3'],
            'values': ['6<sep>4<sep>5<sep>6', '6<sep>5']
        })

        expected_vocabulary = {
            '<pad>': 0, '<mask>': 1, '<sos>': 2, '<eos>': 3, 'A10AB02': 4, 'E11': 5, 'A10AB01': 6
        }
        vocab = HealthEventsVocab(categories_blacklist=None)
        vocab.load_from_json(path2output_dir + 'vocabulary.json')
        print(sequences)
        print(expected__sequences)

        print(vocab.value2ix)
        print(expected_vocabulary)

        assert (
                sequences.equals(expected__sequences) &
                (vocab.value2ix == expected_vocabulary)
        )
