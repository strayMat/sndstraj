import os

import numpy as np
import pandas as pd
from pyspark.sql.types import (StructField, StringType, StructType, DoubleType, TimestampType)

from eventsTransformers.pmiMatrix import build_cooccurrence_matrix
from pyspark_tests import PySparkTest, PATH2TMP

test_events_df = pd.DataFrame({
    'patientID': ['p1', 'p1', 'p1', 'p1', 'p3', 'p3'],
    'category': ['drug', 'drug', 'mco_main_diagnosis', 'mco_main_diagnosis', 'drug',
                 'mco_main_diagnosis'],
    'value': ['A10AB01', 'A10AB02', 'E11', 'A10AB01', 'A10AB01', 'E11'],
    'groupID': ['10000', '10000', '10000', '10000', '10000', '10000'],
    'weight': [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
    'start': ['2019-01-01', '2019-01-10', '2019-01-11', '2019-03-05', '2019-06-01', '2019-07-01'],
    'end': ['2019-01-01', '2019-01-10', '2019-01-11', '2019-03-05', '2019-06-01', '2019-07-01']},
)

test_events_df['start'] = pd.to_datetime(test_events_df['start'])
test_events_df['end'] = pd.to_datetime(test_events_df['end'])

data_schema = [
    StructField('patientID', StringType(), True),
    StructField('category', StringType(), True),
    StructField('value', StringType(), True),
    StructField('groupID', StringType(), True),
    StructField('weight', DoubleType(), True),
    StructField('start', TimestampType(), True),
    StructField('end', TimestampType(), True)]

final_struc = StructType(fields=data_schema)


class Testcohort(PySparkTest):
    def test_build_cooccurrence_matrix(self):
        test_events = self.spark.createDataFrame(test_events_df, schema=final_struc)
        test_events.show()
        test_events.printSchema()

        cooccurrence_matrix, item_count, label2ix = build_cooccurrence_matrix(
            test_events,
            output_dir=os.path.join(PATH2TMP, 'test_cooccurrence'),
            radius_in_days=30,
            window_orientation='centered')
        print(cooccurrence_matrix)
        print(item_count)
        print(label2ix)

        expected__item_count = np.array([3, 2, 1])
        expected__cooccurrence_matrix = np.array(
            [[0.0, 2.0, 1.0], [2.0, 0.0, 1.0], [1.0, 1.0, 0.0]])

        print(expected__cooccurrence_matrix)
        print(expected__item_count)

        print(item_count.dtype)
        print(cooccurrence_matrix.dtype)
        assert (item_count == expected__item_count).all() \
               & (cooccurrence_matrix == expected__cooccurrence_matrix).all()
