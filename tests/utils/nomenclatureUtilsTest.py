import unittest

from utils.nomenclatureUtils import get_concepts_labels, RESOURCES_PATH

class MyTestCase(unittest.TestCase):
    def test_something(self):
        labels_df = get_concepts_labels(['A02BC', '9105', '13.04.03.08', 'E11'], atc_level=4)
        labels = list(labels_df['concept_name'].values)
        expected_labels = [
            'Proton pump inhibitors',
            'Correction des malformations congénitales de la main',
            'diabète sucré de type 2',
            "forfait de securite pour le traitement d'un   echantillon sanguin dans les conditions   prevues par"
        ]
        print(labels)
        print(expected_labels)
        self.assertEqual(labels, expected_labels)


if __name__ == '__main__':
    unittest.main()
