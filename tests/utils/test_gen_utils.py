import pyspark.sql.functions as func

from utils.helpers import build_vocabulary, start_sc

# should be started only once for all tests
sqlcontext = start_sc(4)


def test_build_vocabulary(sqlcontext):
    patients_test = sqlcontext.read.parquet('../data/sample10.parquet')
    label2ix, ix2label = build_vocabulary(
        events_table=patients_test.filter(func.col('patientID') == 'pID-0'),
        event_types=['main_diagnostic', 'dcir_act'])
    expected_label2ix = {'<start>': 0, '<stop>': 1, '<pad>': 2, 'GLQP003': 3,
                         'J441': 4, 'J440': 5, 'GLQP002': 6, 'GLQP009': 7, 'J960': 8, 'J448': 9}

    try:
        assert expected_label2ix == label2ix
    except AssertionError:
        print('Expected : '.format(expected_label2ix))
        print('Given : '.format(label2ix))
        raise


test_build_vocabulary(sqlcontext)
