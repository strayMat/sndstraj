import os

import pandas as pd

from pyspark_tests import PySparkTest, events_structure, PATH2TMP
from utils.HealthEventsVocab import HealthEventsVocab

events = pd.DataFrame({
    'patientID': ['p1', 'p1', 'p1', 'p1', 'p3', 'p3', 'p4', 'p3'],
    'category': ['drug', 'drug', 'mco_main_diagnosis', 'mco_main_diagnosis', 'drug',
                 'mco_main_diagnosis', 'drug', 'ccam_act'],
    'value': ['A10AB01', 'A10AB02', 'E11', 'A10AB01', 'A10AB01', 'E11', 'A10AB01', 'CARA005'],
    'groupID': ['10000', '10000', '10000', '10000', '10000', '10000', '1000', '1000'],
    'weight': [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
    'start': ['2019-01-01', '2019-01-10', '2019-01-11', '2019-03-05', '2019-06-01', '2019-07-01',
              '2019-12-01', '2020-01-01'],
    'end': ['2019-01-01', '2019-01-10', '2019-01-11', '2019-03-05', '2019-06-01', '2019-07-01',
            '2019-12-01', '2020-01-01']
})

events['start'] = pd.to_datetime(events['start'])
events['end'] = pd.to_datetime(events['end'])

path2output_dir = os.path.join(
    PATH2TMP,
    'preprocessed_codes_sequences/'
)


class HealthEventsVocabTest(PySparkTest):

    def test_health_events_vocab(self):
        events_df = self.spark.createDataFrame(events, schema=events_structure)
        events_df.show()
        events_df.printSchema()

        vocab = HealthEventsVocab(
            categories_blacklist=['ccam_act']
        )

        vocab.build_vocab_from_events(
            events=events_df,
            with_deltas=False
        )

        expected_label2ix = {
            '<sos>': 2, '<eos>': 3, '<pad>': 0, '<mask>': 1, 'A10AB02': 4, 'E11': 5, 'A10AB01': 6
        }

        self.assertEqual(expected_label2ix, vocab.value2ix)

    def test_vocab_save_load(self):
        events_df = self.spark.createDataFrame(events, schema=events_structure)
        events_df.show()
        events_df.printSchema()

        vocab = HealthEventsVocab(
            categories_blacklist=['ccam_act']
        )

        vocab.build_vocab_from_events(
            events=events_df,
            with_deltas=False
        )

        path2save = path2output_dir + 'health_vocab.json'
        vocab.save_to_json(path2vocab=path2save)

        loaded_voca = HealthEventsVocab()
        loaded_voca.load_from_json(path2save)
        print(vocab.ix2value)
        self.assertEqual(loaded_voca.ix2value, vocab.ix2value)
        self.assertEqual(loaded_voca.value2ix, vocab.value2ix)
