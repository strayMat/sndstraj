import json
import os
import pickle
import re
from datetime import datetime
from typing import Dict, List

import numpy as np
import plotly
import plotly.graph_objects as go
import scipy.sparse as sparse
from factor_analyzer import Rotator
from sklearn.decomposition import TruncatedSVD, FactorAnalysis
from sklearn.feature_extraction.text import TfidfTransformer

from sndstraj.utils.nomenclatureUtils import get_concepts_labels


def compute_lsa(
        path2bow: str,
        n_components: int = 100,
        random_state: int = 42):
    """
    Given a Bag of Word path, compute Latent Semantic Analysis with [scikit-learn implementation](https://scikit-learn.org/stable/modules/generated/sklearn.decomposition.TruncatedSVD.html)
    This method should easily scale because we the scikit-learn TruncatedSvd takes sparse matrices
     as input.
    :param path2bow:
    :param n_components:
    :param random_state:
    :return:
        bow, original Bag Of Events
        svd, fitted SVD,
        x_reduced, reduced transposed input data,
        label2ix, dictionary with format {code_label : code_ix} where the indices are those in the
        Bag Of Events,
        patient2ix, dictionary with the format {patientID: patientIndexInTheReducedMatrix}
        metadata, supplementary information on the transformation
    """
    metadata = {}
    # load bow matrix, vocabulary and patientIDs
    bow = sparse.load_npz(os.path.join(path2bow, 'sparse_bow.npz'))
    with open(os.path.join(path2bow, 'vocabulary.json')) as f:
        label2ix = json.load(f)
    with open(os.path.join(path2bow, 'patientID2ix.json')) as f:
        patient2ix = json.load(f)

    tf_idf_transformer = TfidfTransformer(smooth_idf=False)
    t0 = datetime.now()
    sparse_tfidf = tf_idf_transformer.fit_transform(bow)
    t1 = datetime.now()
    print("Tf-idf computed in {}".format(t1 - t0))

    bow_sparsity = 1 - (bow >= 1).sum() / (bow.shape[0] * bow.shape[1])
    cov_bow = bow.transpose().dot(bow)
    cov_sparsity = 1 - (cov_bow >= 1).sum() / (cov_bow.shape[0] ** 2)
    # run lsa
    t0 = datetime.now()
    svd = TruncatedSVD(n_components=n_components, n_iter=10, random_state=random_state)
    x_reduced = svd.fit_transform(sparse_tfidf)
    t1 = datetime.now()
    print("SVD computed in {}".format(t1 - t0))

    metadata['timing'] = t1 - t0
    metadata['bow_sparsity'] = bow_sparsity
    metadata['cov_sparsity'] = cov_sparsity

    return bow, svd, x_reduced, label2ix, patient2ix, metadata


def compute_efa(
        path2bow: str,
        rotation: str = None,
        n_components: int = 100,
        random_state: int = 43,
        size_limit: float = 40):
    """
    :param path2bow:
    :param rotation: orthogonal rotations ('varimax', 'oblimax', 'quartimax', 'equamax') or
    oblique rotations ('promax', 'oblimin', 'oblimax', 'quartimin')
    :param n_components:
    :param random_state: 
    :param size_limit: upper size limite of the dense observation matrix (in GB if the matrix
    is a np.float32 matrix)
    :return:
    """
    metadata = {}
    # load bow matrix, vocabulary and patientIDs
    bow = sparse.load_npz(os.path.join(path2bow, 'sparse_bow.npz'))
    with open(os.path.join(path2bow, 'vocabulary.json')) as f:
        label2ix = json.load(f)
    with open(os.path.join(path2bow, 'patientID2ix.json')) as f:
        patient2ix = json.load(f)

    tf_idf_transformer = TfidfTransformer(smooth_idf=False)
    t0 = datetime.now()
    sparse_tfidf = tf_idf_transformer.fit_transform(bow)
    t1 = datetime.now()
    print("Tf-idf computed in {}".format(t1 - t0))

    bow_sparsity = 1 - (bow >= 1).sum() / (bow.shape[0] * bow.shape[1])
    cov_bow = bow.transpose().dot(bow)
    cov_sparsity = 1 - (cov_bow >= 1).sum() / (cov_bow.shape[0] ** 2)
    # run lsa
    t0 = datetime.now()
    efa = FactorAnalysis(n_components=n_components, random_state=random_state, max_iter=1000)
    if (sparse_tfidf.shape[0] * sparse_tfidf.shape[1]) <= size_limit * 1e9 / 4:
        dense_tfidf = bow.todense()
    else:
        raise 'Too many dimensions to cast sparse into dense matrix : dims are {}'.format(
            sparse_tfidf.shape)
    x_reduced = efa.fit_transform(dense_tfidf)
    t1 = datetime.now()
    print("EFA computed in {}".format(t1 - t0))

    if rotation is not None:
        t2 = datetime.now()
        rotator = Rotator(method=rotation)
        efa.components_ = rotator.fit_transform(efa.components_.transpose()).transpose()
        x_reduced = x_reduced.dot(rotator.rotation_)
        t3 = datetime.now()
        print('rotation in {}'.format(t3 - t2))
        metadata['timing'] = (t1 - t0) + (t3 - t2)
        metadata['rotation'] = rotator.rotation_
    else:
        metadata['timing'] = (t1 - t0)
    metadata['bow_sparsity'] = bow_sparsity
    metadata['cov_sparsity'] = cov_sparsity
    return bow, efa, x_reduced, label2ix, patient2ix, metadata


# Unused for now
def remove_colinearities(
        tfidf: np.array,
        label2ix,
        verbose: str = 1) -> (np.array, Dict):
    """
    Wee remove the colinear features in the input data.
    This can be useful when using exact solvers for factor analysis.
    I think that it is not crucial when using the scikitlearn SVD solver.
    """
    unique_tfidf, unique_ix = np.unique(tfidf, axis=1, return_index=True)
    # inverting the vocabulary
    ix2label = {v: k for (k, v) in label2ix.items()}
    # getting only unique ix (remving colinear in the vocabulary)
    new_label2ix = {ix2label[ix]: ix for ix in unique_ix}
    if verbose:
        removed = [ix2label[ix] for ix in (set(ix2label.keys()) - set(unique_ix))]
        print('removing colinear terms : {}'.format(removed))
    return unique_tfidf, new_label2ix


def save_decomposition_exp(
        dir2save: str,
        decomposition_name: str,
        bow,
        decomposition_results,
        x_reduced: np.ndarray,
        label2ix: Dict,
        metadata: Dict,
        rotation: str = None,
        n_sample: int = 10000,
        random_state: int = 42) -> Dict:
    """
    :return: 
    Dictionary of results as expected by the R package LDAvis
    decomposition_dict = {
        'phi': components (nb_components x vocab_size), 
        'theta': sample of projected documents (nb_docs x nb_components), 
        'doc_length': nb of terms per document (nb_docs),
        'vocab': labels of the vocabulary terms (vocab_size), 
        'term_freq': term frequencies in sample (vocab_size),
        'metadata':
            {
                'timing': time taken by the decomposition
                'bow_sparsity': sparsity of the observation matrix,
                'cov_sparsity': sparsity of the covariance matrix,
                'n_components': number of components for the decomposition
                'n_total_patients': total number of patients
            }
    }
    """
    assert decomposition_name in ['lsa', 'lda', 'efa'], 'Wrong name for decomposition!'
    decomposition_dict = {}
    n_patients = x_reduced.shape[0]
    n_components = x_reduced.shape[1]
    decomposition_dict['topic_term_dists'] = decomposition_results.components_
    np.random.seed(random_state)
    random_indices = np.random.choice(np.arange(n_patients), n_sample, replace=False)
    decomposition_dict['doc_topic_dists'] = x_reduced[random_indices, :]
    decomposition_dict['doc_lengths'] = np.asarray(np.sum(bow[random_indices, :], axis=1)).squeeze()
    decomposition_dict['vocab'] = list(label2ix.keys())
    term_counts = np.asarray(np.sum(bow[random_indices, :], axis=0)).squeeze()
    decomposition_dict['term_frequencies'] = term_counts / term_counts.sum()
    decomposition_dict['metadata'] = dict(
        **metadata,
        **{
            'n_components': n_components,
            'n_total_patients': n_patients
        }
    )
    if rotation is None:
        rotation = ''
    decomposition_dict['sample_indices'] = random_indices
    path2save = os.path.join(dir2save, '{}_results.obj'.format(rotation + '_' + decomposition_name))
    print('Saving results at {}'.format(path2save))
    with open(path2save, 'wb') as f:
        pickle.dump(decomposition_dict, f)
    return decomposition_dict


def run_save_decomposition(
        path2bow,
        decomposition_name: str,
        rotation: str = None,
        n_components: int = 100,
        random_state: int = 42,
        n_sample: int = 10000,
        size_limit: int = 40) -> Dict:
    if decomposition_name == 'lsa':
        bow, factorization_results, x_reduced, label2ix, patient2ix, metadata = compute_lsa(
            path2bow=path2bow,
            n_components=n_components,
            random_state=random_state
        )
    elif decomposition_name == 'efa':
        bow, factorization_results, x_reduced, label2ix, patient2ix, metadata = compute_efa(
            path2bow=path2bow,
            rotation=rotation,
            n_components=n_components,
            random_state=random_state,
            size_limit=size_limit
        )
    elif decomposition_name == 'lda':
        raise NotImplementedError
    else:
        raise ValueError('Please enter a decomposition chosen among [lsa, efa, lda]')

    decomposition_dict = save_decomposition_exp(
        dir2save=path2bow,
        decomposition_name=decomposition_name,
        bow=bow,
        decomposition_results=factorization_results,
        x_reduced=x_reduced,
        label2ix=label2ix,
        metadata=metadata,
        rotation=rotation,
        n_sample=n_sample,
        random_state=random_state
    )
    return decomposition_dict


def load_decomposition_model(filename: str) -> Dict:
    """
    Description : Loading and normalization of the data to fit into easily interpretables score
    matrices of shape (n_components x n_factors) and (n_samples x n_components) where each row
    belongs to the simplex $R^{nComponents -1}$ ie. with each cell >= 0 and row summing to one.
    """
    with open(filename, 'rb') as f:
        lsa_dict = pickle.load(f)
    data = {
        'topic_term_dists': np.abs(lsa_dict['topic_term_dists']) / np.abs(lsa_dict['topic_term_dists']).sum(axis=1,
                                                                                  keepdims=1),
        # normalization for ldavis in softmax
        'doc_topic_dists': np.abs(lsa_dict['doc_topic_dists']) / np.abs(lsa_dict['doc_topic_dists']).sum(axis=1,
                                                                                     keepdims=1),
        # normalization for ldavis in softmax
        'doc_lengths': lsa_dict['doc_lengths'],
        'vocab': lsa_dict['vocab'],
        'term_frequency': lsa_dict['term_frequencies'],
        'metadata': lsa_dict['metadata'],
        'sample_indices': lsa_dict['sample_indices']
    }
    return data


def plot_explained_variance(
        group_names: List[str],
        path2save: str,
        n_components: int = None,
        verbose: int = 1):
    bars = []
    for group_name in group_names:
        filename = os.path.join(group_name, 'lsa_results.obj')
        group_regex = re.search('\_((\d\d)-(\d\d*)\_\w)', group_name)
        if group_regex is None:
            group_label = ''
        else:
            group_label = group_regex.group(1)
        data = load_decomposition_model(filename)
        yy = data['metadata']['svd_ratio_explained']
        xx = np.arange(len(yy)) + 1
        if n_components is not None:
            xx = xx[:n_components]
            yy = yy[:n_components]
        if verbose:
            if n_components is None:
                n_components = len(yy)
            print(
                'For group {}, the total variance ratio explained by {} components is : {}'.format(
                    group_label, n_components, yy.sum()
                ))
        bars.append(go.Bar(x=xx, y=yy, name=group_label))
    fig = go.Figure(
        bars
    )
    fig.update_layout(barmode='group')
    fig.write_html(path2save)
    return


def plot_top_component_terms(
        path2result: str,
        dir2save: str,
        n_components: int,
        n_terms: int,
        custom_color_scale: bool = False):
    """
    Select the terms that have the higher support on the the first n chosen components and sort all
    terms composing the components according to these n terms.
    :param path2result:
    :param dir2save:
    :param n_components:
    :param n_terms:
    :param custom_color_scale:
    :return:
    """
    data = load_decomposition_model(path2result)
    group_label = re.search('\_((\d\d)-(\d\d*)\_\w)', path2result).group(1)
    decomposition_type = re.search('\/(\w\w*)\_results.obj', path2result).group(1)

    # we try to select important term and top_components 
    top_components = data['topic_term_dists'][
                     :n_components]  # on enleve le composant 0 qui semble être la moyenne
    top_terms_ix = np.argsort(top_components.sum(axis=0))[-n_terms:][
                   ::-1]  # get the top n_terms that enter in the composition of the these 30 top components
    top_terms_percentage = top_components.sum(axis=0)[top_terms_ix] / top_components.shape[
        0]  # on recupere les pourcentages de composition moyen sur les 30 premiers concepts
    top_components_terms = top_components[:,
                           top_terms_ix]  # on recupere la matrice d'intensité pour ces termes et ces composants

    y_labels = ['comp_{}'.format(ix) for ix in np.arange(n_components)]
    x_codes = np.array(data['vocab'])[top_terms_ix]

    # get labels
    x_labels_df = get_concepts_labels(study_codes=list(x_codes), atc_level=4)
    x_labels_as_dict = x_labels_df.set_index('concept_code')['concept_name'].to_dict()
    x_labels = [str(c) + ' : ' + str(x_labels_as_dict[c])[:50] for c in x_codes]

    path2save = os.path.join(dir2save, '{}_top_components-terms_{}.html'.format(decomposition_type,
                                                                                group_label))
    data = [{
        'z': top_components_terms,
        'x': x_labels,
        'y': y_labels,
        'hoverongaps': False,
        'type': 'heatmap'
    }]
    # custom_color_scale not working...
    if custom_color_scale:
        thres_holds = np.percentile(top_components_terms, q=[70, 95, 99, 99.5, 100])
        data[0]['colorscale'] = [
            [0, 'rgb(69, 117, 180)'],
            [thres_holds[0], 'rgb(116, 173, 209)'],
            [thres_holds[1], 'rgb(171, 217, 233)'],
            [thres_holds[2], 'rgb(254, 224, 144)'],
            [thres_holds[3], 'rgb(244, 109, 67)'],
            [thres_holds[4], 'rgb(165, 0, 38)']
        ]
        data[0]['colorbar'] = {'tick0': 0, 'tickmode': 'array', 'tickvals': [0] + list(thres_holds)}
    fig = go.Figure(
        data
    )
    fig.update_layout(
        title='{} top variables in {} top {} components explaining group comorbidities : {}'.format(
            n_terms, n_components, decomposition_type, group_label),
        xaxis_title='top {} variables entering in the compositions of the top {} components'.format(
            n_terms, n_components),
        yaxis_title='top {} components'.format(n_components)
    )
    fig.update_xaxes(tickangle=45)
    # config={'scrollZoom': True}

    plotly.offline.plot(fig, filename=path2save)
    return


def plot_top_terms(
        path2result: str,
        dir2save: str,
        n_components: int,
        n_terms: int):
    """
    For each top n component, plot the top n terrms with their absolute support showed on the y axis.
    :param path2result:
    :param dir2save:
    :param n_components:
    :param n_terms:
    :return:
    """
    data = load_decomposition_model(path2result)
    group_regex = re.search('\_((\d\d)-(\d\d*)\_\w)', path2result)
    if group_regex is None:
        group_label = ''
    else:
        group_label = group_regex.group(1)
    decomposition_type = re.search('\/(\w\w*)\_results.obj', path2result).group(1)

    all_labels = get_concepts_labels(study_codes=data['vocab'], atc_level=4, verbose=1)
    all_labels_dict = all_labels.set_index('concept_code')['concept_name'].to_dict()
    plotted_data = []
    for ix in np.arange(n_components):
        chosen_component = data['topic_term_dists'][ix]
        best_ix = chosen_component.argsort()[::-1][:n_terms]
        component_sorted = chosen_component[best_ix]
        component_codes = np.array(data['vocab'])[best_ix]
        component_labels = [str(c) + ' : ' + str(all_labels_dict[c])[:100] for c in component_codes]
        plotted_component = {}
        plotted_component['x'] = np.array(np.repeat(ix, n_terms))[::-1]
        plotted_component['y'] = component_sorted[::-1]
        plotted_component['label'] = component_labels[::-1]
        plotted_data.append(plotted_component)

    scatters = []
    for component_top_k_terms in plotted_data:
        scatters.append(
            go.Scatter(
                x=component_top_k_terms['x'],
                y=component_top_k_terms['y'],
                mode='markers',
                marker_symbol='x',
                marker=dict(size=10),
                hoverinfo=['x', 'y', 'text'],
                text=component_top_k_terms['label'],
            )
        )
    fig = go.Figure(
        scatters
    )
    fig.update_layout(
        title='{} top variables in {} top {} components explaining group comorbidities : {}'.format(
            n_terms, n_components, decomposition_type, group_label),
        xaxis_title='top {} variables entering in the compositions of the top {} components'.format(
            n_terms, n_components),
        yaxis_title='top {} components'.format(n_components)
    )
    if not os.path.isdir(dir2save):
        os.mkdir(dir2save)
    path2save = os.path.join(dir2save,
                             '{}_top_terms_per_component_{}.html'.format(decomposition_type,
                                                                         group_label))
    plotly.offline.plot(fig, config={'scrollZoom': True}, filename=path2save)
    return fig
