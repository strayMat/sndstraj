import networkx as nx
from pyspark.sql import DataFrame
from typing import List, Dict
import pyspark.sql.functions as func
import pandas as pd
import numpy as np


class Cohort:
    """
    Description: an abstract class for the cohort object
    """

    def __init__(
            self,
            name: str,
            events: DataFrame,
            patients: DataFrame = None,
            characteristics: str = '',
            color: str = 'skyblue',
            population_key: List[str] = None
    ):
        self.name = name
        self.characteristics = characteristics
        self.events = events
        self.population_size = 'not_computed',
        self.color = color
        if population_key is not None:
            self.population_key = population_key
        else:
            self.population_key = ['patientID']

    def execute_computation(self) -> None:
        """
        Description: Because Spark is lazy, we need to call count to execute the computation
        :return:
        """
        # caching aggressively all events
        self.events = self.events.cache()
        # do computation
        self.population_size = self.events.select(self.population_key).distinct().count()

    def add_to_flow_chart(self, g: nx.DiGraph) -> None:
        return

    def get_population_description(
            self,
            irbenr: DataFrame,
            percentiles: np.array = None,
            demographic_cols: List = None) -> Dict:
        """
        Description: Compute demographic informations for the population cohort
        :param irbenr: A dataframe derived from irbenr with some supplementary columns:
            DataFrame['patientID', 'BEN_RES_DPT_int', 'age', 'BEN_NAI_ANN', 'BEN_SEX_COD', 'regime_code', 'BEN_CMU_TOP_ANN']
            where :
                BEN_RES_DPT_int is the clean version of ben_res_dpt
                age is  the age taken with respect to a reference year
                regime_code is the 3 first characters of ORG_AFF_BEN
        :param percentiles: Percentiles for quantitative variables
        :param demographic_cols: Can be used if your irbenr does not have all snds demographic informations
        :return:
        """
        if percentiles is None:
            percentiles = np.array([0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.99])
        if demographic_cols is None:
            demographic_cols = ['patientID', 'BEN_RES_DPT_int', 'age', 'BEN_NAI_ANN', 'BEN_SEX_COD',
                                'regime_code', 'BEN_CMU_TOP_ANN']
        results = {}
        patient_infos = irbenr.select(demographic_cols).join(
            self.events.select("patientID").distinct(), "patientID", "inner")
        patient_ages_infos = patient_infos.select('BEN_SEX_COD', 'age').toPandas()

        if 'BEN_CMU_TOP_ANN' in demographic_cols:
            results['cmuc'] = patient_infos.select('BEN_CMU_TOP_ANN', 'patientID').groupBy(
                'BEN_CMU_TOP_ANN').count().sort(func.col('BEN_CMU_TOP_ANN')).toPandas().set_index(
                'BEN_CMU_TOP_ANN').transpose()

        if 'BEN_SEX_COD' in demographic_cols:
            results['sexes'] = patient_infos.select('BEN_SEX_COD', 'patientID').groupBy(
                'BEN_SEX_COD').count().sort(func.col('BEN_SEX_COD')).toPandas().set_index(
                'BEN_SEX_COD').transpose().rename(
                columns={1: 'homme', 2: "femme", 0: 'inconnu'})

        if 'BEN_RES_DPT_int' in demographic_cols:
            results['departements'] = patient_infos.groupBy('patientID').agg(
                func.last(func.col('BEN_RES_DPT_int')).alias('BEN_RES_DPT_int')).groupBy(
                'BEN_RES_DPT_int').count().sort(func.col('BEN_RES_DPT_int')).toPandas().set_index(
                'BEN_RES_DPT_int').transpose()

        if 'regime_code' in demographic_cols:
            results['regimes'] = patient_infos.groupBy('patientID').agg(
                func.last(func.col('regime_code')).alias('regime_code')).groupBy(
                'regime_code').count().sort(func.col('regime_code')).toPandas().set_index(
                'regime_code').transpose()

        if 'age' in demographic_cols:
            results['ages'] = pd.DataFrame(
                patient_ages_infos['age'].describe(percentiles=percentiles)).transpose()

            results['ages_femmes'] = pd.DataFrame(patient_ages_infos.loc[
                                                      patient_ages_infos[
                                                          'BEN_SEX_COD'] == 2, 'age'].describe(
                percentiles=percentiles)).transpose()
            results['ages_hommes'] = pd.DataFrame(patient_ages_infos.loc[
                                                      patient_ages_infos[
                                                          'BEN_SEX_COD'] == 1, 'age'].describe(
                percentiles=percentiles)).transpose()

        return results


class SourceCohort(Cohort):
    def __init__(
            self,
            name: str,
            events: DataFrame,
            patients: DataFrame = None,
            characteristics: str = '',
            color: str = 'lightgreen',
            population_key: List[str] = None
    ):
        """
        Description: Initialize a source cohort that does not depend on any previous cohort
        :param name:
        :param events: Events tables with following columns:
            ["patientID", "category", "value", "groupID", "weight", "start", "end"]
        :param characteristics: Brief description
        """
        super().__init__(name, events, patients, characteristics, color, population_key)

    def add_to_flow_chart(self, flow_chart: nx.DiGraph) -> None:
        """
        Description: add cohort information to a flowchart
        :param flow_chart: directed graph that contains the flowchart logic as
        a succession of cohorts
        :return:
        """
        flow_chart.add_node(
            self.name,
            label=self.name + ' :\n {} '.format(self.population_size),
            color=self.color)
        return


class FilteredCohort(Cohort):

    def __init__(
            self,
            name: str,
            events: DataFrame,
            parent: Cohort,
            patients: DataFrame = None,
            characteristics: str = '',
            filtered_label: str = '',
            color: str = 'skyblue',
            population_key: List[str] = None
    ) -> None:
        """
        Description: Initialize a filtered cohort that does depend on one previously created cohort
        :param name:
        :param events: Events tables with following columns:
            ["patientID", "category", "value", "groupID", "weight", "start", "end"]
            This should be the events table from the parent cohort on which some filters have been
            applied
        :param parent: The parent cohort on which this cohort depend
        :param characteristics: Brief description
        """
        super().__init__(name, events, patients, characteristics, color, population_key)
        self.parent = parent
        self.filtered_label = filtered_label
        return

    def add_to_flow_chart(self, flow_chart: nx.DiGraph) -> None:
        """
        Description: add cohort information to a flowchart
        :param flow_chart: directed graph that contains the flowchart logic as
        a succession of cohorts
        :return:
        """
        flow_chart.add_node(
            self.name,
            label=self.name + ' :\n {} '.format(self.population_size),
            color=self.color)
        flow_chart.add_edge(self.parent.name, self.name, operation='filter ' + self.filtered_label)
        return


class OperationCohort(Cohort):

    def __init__(
            self,
            name: str,
            left_parent: Cohort,
            right_parent: Cohort,
            operation_type: str,
            patients: DataFrame = None,
            characteristics: str = '',
            color: str = 'skyblue',
            population_key: List[str] = None
    ) -> None:
        """
         Description: Initialize an operation cohort that results from an operation on two parent
         columns.
        :param name:
        :param left_parent:
        :param right_parent:
        :param operation_type: Only three operations are allowed:
            - 'intersection': union all events from patients present in both cohorts,
            - 'union': union all events and patients from both cohorts,
            -'exclusion': exclude patients of the right_parent from the left_parent

        :param characteristics:
        """
        self.left_parent = left_parent
        self.right_parent = right_parent
        self.operation_type = operation_type
        self.patients = patients
        self.available_operations = ['intersection', 'exclusion', 'union']
        self.population_size = 'not_computed',
        self.color = color
        if population_key is not None:
            self.population_key = population_key
        else:
            self.population_key = ['patientID']

        if operation_type not in self.available_operations:
            raise ValueError('Operation type should be in {}'.format(self.available_operations))

        self.name = name

        if characteristics == '':
            self.characteristics = '(' + left_parent.characteristics + \
                                   self.operation_type + right_parent.characteristics + ')'
        else:
            self.characteristics = characteristics

        if self.operation_type == 'exclusion':
            self.events = left_parent.events.join(
                right_parent.events.select(self.population_key),
                on=self.population_key,
                how='left_anti')
        elif self.operation_type == 'intersection':
            patients_intersection = DataFrame.intersect(
                left_parent.events.select(self.population_key),
                right_parent.events.select(self.population_key)).distinct()
            # too computation intensive ?
            self.events = DataFrame.union(
                left_parent.events,
                right_parent.events).join(
                patients_intersection,
                self.population_key,
                'inner').distinct()
        elif self.operation_type == 'union':
            self.events = DataFrame.union(
                left_parent.events,
                right_parent.events).distinct()
        else:
            raise ValueError('Operation not allowed')

        return

    def add_to_flow_chart(self, flow_chart: nx.DiGraph) -> None:
        """
        Description: add cohort information to a flowchart
        :param flow_chart: directed graph that contains the flowchart logic as
        a succession of cohorts
        :return:
        """
        flow_chart.add_node(
            self.name,
            label=self.name + ' :\n {} '.format(self.population_size),
            color=self.color)
        if self.operation_type == 'exclusion':
            left_label = 'included'
            right_label = 'excluded'
        else:
            left_label = self.operation_type
            right_label = self.operation_type
        flow_chart.add_edge(
            self.left_parent.name,
            self.name,
            operation=left_label)
        flow_chart.add_edge(
            self.right_parent.name,
            self.name,
            operation=right_label)
        return
