from datetime import datetime
from typing import List, Dict

import matplotlib.pyplot as plt
import pandas as pd
import pyspark.sql.functions as func
from pyspark.sql import DataFrame

from sndstraj.utils.nomenclatureUtils import CONCEPTS_PATH, IR_PHA_PATH
SEED = 12


def describe_cohort(
        cohort: DataFrame,
        described_fields: Dict,
        top_k=10,
        percentiles: List = None,
        path2irpha=IR_PHA_PATH,
        path2concepts=CONCEPTS_PATH,
        verbose=1) -> Dict:

    # TODO separer les differentes description en unites fonctionnelles
    if percentiles is None:
        percentiles = [0.01, 0.25, 0.5, 0.75, 0.99]
    t0 = datetime.now()
    # load referentiels to have code labels
    concepts = pd.read_csv(path2concepts)
    irPha = pd.read_csv(path2irpha, dtype={'PHA_PRS_C13': 'int'}, sep=';')
    # general descriptors
    description = {}
    nb_events = cohort.count()
    description['nb_events'] = nb_events
    nb_events_per_patients = cohort.groupBy('patientID').count().select('count').toPandas()
    nb_patients = nb_events_per_patients.shape[0]
    description['nb_patients'] = nb_patients

    nb_events_described = nb_events_per_patients.describe(percentiles=percentiles)
    description['nb_events_described'] = nb_events_described

    if verbose:
        print('Cohort with {} patients and {} events'.format(nb_patients, nb_events))
    # compute a detailled view on each category of event
    if 'category_of_events' in described_fields.keys():
        print('Computing nb_category_of_events per patients')
        nb_category_of_events = cohort.groupBy('patientID').pivot('category').count().fillna(
            0).toPandas().describe(percentiles=percentiles)
        description['nb_category_of_events'] = nb_category_of_events

    if 'categorical_top_k' in described_fields.keys():
        if verbose:
            print('Computing top_k with k = {}'.format(top_k))
        top_k_fields = {}
        for cat, code_level in described_fields['categorical_top_k'].items():
            if verbose >= 2:
                print('Computing top {} codes in {} category'.format(top_k, cat))
            top_k_cat = cohort.filter(func.col('category') == cat).groupby(
                func.col('value')).count().sort(func.desc('count')).toPandas()
            # bancal
            if cat == 'drug':
                concepts_drug = concepts.loc[
                    concepts['vocabulary_id'] == 'SNDS - cip13', ['concept_name', 'concept_code']]
                concepts_drug.loc[:, 'concept_code'] = concepts_drug.loc[:, 'concept_code'].map(
                    lambda x: int(x.split('.')[0]))
                concepts_w_irPha = concepts_drug.merge(
                    irPha.loc[:, ['PHA_CIP_C13', 'PHA_ATC_C07', 'PHA_ATC_L07', 'PHA_ATC_L03']],
                    left_on='concept_code', right_on='PHA_CIP_C13', how='left').drop(
                    ['PHA_CIP_C13', 'concept_name', 'concept_code'], axis=1)
                concepts_libs = concepts_w_irPha.rename(columns={'PHA_ATC_C07': 'concept_code'})
                concepts_libs_unique = concepts_libs.drop_duplicates()
            else:
                concepts_libs = concepts.loc[
                    concepts['concept_code'].str.len() == code_level, ['concept_name',
                                                                       'concept_code']]
                concepts_libs_unique = concepts_libs.drop_duplicates()

            top_k_cat_w_libelles = top_k_cat.merge(concepts_libs_unique, left_on='value',
                                                   right_on='concept_code', how='left').drop(
                ['concept_code'], axis=1)
            top_k_cat_renamed = top_k_cat_w_libelles.loc[:top_k, :].rename(
                columns={'value': cat + '_value', 'count': cat + '_count',
                         'concept_name': cat + '_concept_name'})

            top_k_fields[cat] = top_k_cat_renamed

        description['top_k_fields'] = top_k_fields

    print('Computed in {}'.format(datetime.now() - t0))
    if verbose >= 2:
        for k, v in description.items():
            print('Descriptor {}'.format(k))
            print(v)
    return description


def compute_age_bucket_count(gender_table: DataFrame) -> Dict:
    # aggregation sufficiently reduced size to transition to pandas
    gender_bucket_counts = gender_table.groupBy(
        func.col('age_bucket')).agg((func.count('age_bucket')).alias('bucket_count')).toPandas()
    gender_bucket_counts.loc[:, 'bucket_count'] = gender_bucket_counts.loc[:, 'bucket_count']
    gender_bucket_counts.sort_values('age_bucket', inplace=True)
    return gender_bucket_counts


def age_pyramide_plot(
        patients: DataFrame,
        plot: bool = True,
        bucket_size: int = 5,
        reference_date: str = '2019-01-01') -> pd.DataFrame:
    patients_bucketized = add_age(
        patients=patients,
        bucket_size=bucket_size,
        reference_date=reference_date)
    male_age_buckets = compute_age_bucket_count(patients_bucketized.filter(func.col('gender') == 1))
    male_age_buckets['gender'] = 1
    female_age_buckets = compute_age_bucket_count(
        patients_bucketized.filter(func.col('gender') == 2))
    female_age_buckets['gender'] = 2
    age_buckets = pd.concat((male_age_buckets, female_age_buckets))

    if plot:
        fig, (ax1, ax2) = plt.subplots(1, 2, sharey='all', figsize=(9, 5))
        ax1.barh(
            male_age_buckets['age_bucket'] * bucket_size,
            male_age_buckets['bucket_count'] / male_age_buckets['bucket_count'].sum(),
            height=bucket_size,
            label='hommes')
        ax1.invert_xaxis()
        ax2.barh(
            female_age_buckets['age_bucket'] * bucket_size,
            female_age_buckets['bucket_count'] / female_age_buckets['bucket_count'].sum(),
            height=bucket_size,
            label='femmes',
            color='C1')
        # ax2.set_xlim([0, pop_size / 12])
        # fig.suptitle('Pyramide des âges (ref {})'.format(baseline_year))
        ax1.legend(loc='upper left')
        ax2.legend(loc=0)

        plt.show()
    return age_buckets


def describe_demographics(patients: DataFrame) -> Dict:
    """
    Desctiption: Compute basic demographics on the patient cohort
    :param patients: a patient cohort (ages are not necessary)
    :return: dictionnary with results :
        keys = ['female_ratio', 'dcd_ratio', 'population_size', 'date_description']
    """
    patients_ages_spark = patients.select(
        func.year(func.col('birthDate')).alias('birth_year'),
        'gender',
        func.year(func.col('deathDate')).alias('death_year'))

    patients_ages = patients_ages_spark.toPandas()
    female_ages = patients_ages.loc[patients_ages['gender'] == 2, ['birth_year']]
    pop_size = patients_ages.shape[0]
    nb_females = female_ages.shape[0]
    nb_dcd = ((~pd.isna(patients_ages['death_year'])) & (patients_ages['death_year'] <= 2019)).sum()
    female_ratio = nb_females / pop_size
    dcd_ratio = nb_dcd / pop_size
    print('Taille population : {}'.format(pop_size))
    print('Part femmes : {}/{} = {:.4f}'.format(nb_females, pop_size, female_ratio))
    print('Part DCD : {}/{} = {:.4f}'.format(nb_dcd, pop_size, dcd_ratio))
    birth_death_description = pd.DataFrame(
        round(patients_ages.loc[:, ['birth_year', 'death_year']].describe(
            percentiles=[0.01, 0.25, 0.5, 0.75, 0.99])))

    results = {
        'female_ratio': female_ratio,
        'dcd_ratio': dcd_ratio,
        'population_size': pop_size,
        'date_description': birth_death_description
    }
    return results


def add_age(
        patients: DataFrame,
        reference_date: str = '2019-01-01',
        bucket_size: int = 5,
        verbose: int = 0) -> DataFrame:
    """
    Description : Add age and age_bucket to a patient pyspark dataframe
    :param patients: Patient dataframe [patientId, gender, birthDate, deathDate]
    :param reference_date: base year to compute the age
    :param bucket_size: in year
    :param verbose:
    :return: Patient dataframe with ages [patientId, gender, birthDate, deathDate, age, age_bucket]
    """
    if verbose:
        print('Creating age buckets of size {}, ex:\n [0-{}]->')
    patients_w_ages = patients.withColumn(
        'age', (func.datediff(
            func.lit(reference_date), func.col('birthDate')) / 365).cast('long'))
    patients_w_age_buckets = patients_w_ages.withColumn(
        'age_bucket', (func.col('age') / bucket_size).cast('long'))

    return patients_w_age_buckets


def stratified_sampling_on_gender_age(
        control_patients: DataFrame,
        target_patients: DataFrame,
        bucket_size: int = 5,
        target_ratio_fraction: float = 1.0) -> DataFrame:
    """
    Description: Perform stratified sampling to get a proxy on age.
    :param bucket_size:
    :param control_patients:
    :param target_patients:
    :return:
    """

    target_gender_age_buckets = age_pyramide_plot(
        target_patients,
        plot=False,
        bucket_size=bucket_size)
    control_gender_age_buckets = age_pyramide_plot(
        control_patients,
        plot=False,
        bucket_size=bucket_size)

    control_male_bucket_df = control_gender_age_buckets.loc[
        control_gender_age_buckets['gender'] == 1, ['age_bucket', 'bucket_count']].set_index(
        'age_bucket')
    control_female_bucket_df = control_gender_age_buckets.loc[
        control_gender_age_buckets['gender'] == 2, ['age_bucket', 'bucket_count']].set_index(
        'age_bucket')
    target_male_buckets_df = target_gender_age_buckets.loc[
        target_gender_age_buckets['gender'] == 1, ['age_bucket', 'bucket_count']].set_index(
        'age_bucket')
    target_female_buckets_df = target_gender_age_buckets.loc[
        target_gender_age_buckets['gender'] == 2, ['age_bucket', 'bucket_count']].set_index(
        'age_bucket')

    male_ratios = (target_male_buckets_df / control_male_bucket_df).fillna(0)
    male_ratios['bucket_count'] = male_ratios['bucket_count'].map(
        lambda x: min(target_ratio_fraction * x, 1))

    female_ratios = (target_female_buckets_df / control_female_bucket_df).fillna(0)
    female_ratios['bucket_count'] = female_ratios['bucket_count'].map(
        lambda x: min(target_ratio_fraction * x, 1))

    # sampleBy takes a dictionary as input
    male_buckets = male_ratios.to_dict()['bucket_count']
    female_buckets = female_ratios.to_dict()['bucket_count']
    # Conditionally on gender sample ages
    male_stratified_control_patients = add_age(
        control_patients, bucket_size=bucket_size).filter(
        func.col('gender') == 1).sampleBy(
        'age_bucket', male_buckets)
    female_stratified_control_patients = add_age(
        control_patients, bucket_size=bucket_size).filter(
        func.col('gender') == 2).sampleBy(
        'age_bucket', female_buckets)

    stratified_control_patients = male_stratified_control_patients.union(
        female_stratified_control_patients)

    return stratified_control_patients
