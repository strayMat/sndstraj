from collections import OrderedDict
from typing import Dict, List

import matplotlib.pyplot as plt
import networkx as nx

from sndstraj.cohorts.cohort import Cohort


class Indicator:
    def __init__(
            self,
            name: str,
            cohorts: OrderedDict = None,
            results: Dict = None,
    ):
        if results is None:
            results = dict()
        if cohorts is None:
            cohorts = OrderedDict()
        self.name = name
        self.cohorts = cohorts
        self.results = results
        self.numerator_count = 0
        self.denominator_count= 1

    def _add_cohort(self, new_cohort: Cohort):
        self.cohorts[new_cohort.name] = new_cohort

    def add_cohorts(self, cohorts: List[Cohort]):
        for cohort in cohorts:
            self._add_cohort(cohort)

    def execute_dag(self, verbose: int = 1) -> None:
        for cohort_name, cohort in self.cohorts.items():
            if verbose:
                print('Computing {} cohort '.format(cohort_name))
            cohort.execute_computation()
            if verbose:
                print('size= {}'.format(cohort.population_size))
            self.results[cohort_name] = cohort.population_size
        return

    def plot_flow_chart(self, layout='spring', random_seed: str = 12) -> plt.plot:
        flow_chart = nx.DiGraph()
        for cohort_name, cohort in self.cohorts.items():
            cohort.add_to_flow_chart(flow_chart)
        self.flow_chart = flow_chart
        node_labels = nx.get_node_attributes(flow_chart, 'label')
        edge_labels = nx.get_edge_attributes(flow_chart, 'operation')
        node_colors = list(nx.get_node_attributes(flow_chart, 'color').values())
        if layout == 'spring':
            pos = nx.spring_layout(self.flow_chart, scale=1, seed=random_seed)
        else:
            pos = nx.spring_layout(self.flow_chart, scale=1, seed=random_seed)
        nx.draw_networkx_edge_labels(
            G=self.flow_chart,
            pos=pos,
            edge_labels=edge_labels,
            font_size=15)
        flow_plot = nx.draw(
            G=self.flow_chart,
            pos=pos,
            with_labels=True, labels=node_labels,
            font_weight='bold', font_color="black",
            node_size=5000, node_color=node_colors, node_shape="o", alpha=0.8, linewidths=4,
            width=2, arrowsize=25, edge_color="grey")

        return flow_plot

    def compute_taux(
            self,
            numerator: Cohort,
            denominator: Cohort):
        if numerator.population_size == 'not_computed':
            print('Computing numerator population size')
            numerator.execute_computation()
        if denominator.population_size == 'not_computed':
            print('Computing denominator population size')
            denominator.execute_computation()
        self.numerator_count = numerator.population_size
        self.denominator_count = denominator.population_size
        #  validity check on the denonimator_count
        try:
            self.taux = self.numerator_count / self.denominator_count
        except ZeroDivisionError:
            print('ERROR: Denominator population of {} cohort is {}'.format(
                denominator.name,
                self.denominator_count))
        # validity check
        if self.taux > 1:
            raise ValueError('ERROR: taux (pop of {})/ (pop of {}) = {:.4} > 1'.format(
                numerator.name,
                denominator.name,
                self.taux
            ))
        return self.numerator_count, self.denominator_count
