import os

import pandas as pd
import pyspark.sql.functions as func
from pyspark.sql.functions import when
from bokeh.models import HoverTool, WheelZoomTool, PanTool, BoxZoomTool, ResetTool, TapTool, \
    SaveTool
from bokeh.palettes import brewer
from bokeh.plotting import figure, ColumnDataSource, output_file, show, save
from bokeh.transform import jitter
from pyspark.sql import DataFrame

from sndstraj.utils.nomenclatureUtils import add_ATC2concepts, CONCEPTS_PATH, IR_PHA_PATH

PATH2FILE = os.path.dirname(__file__)


def add_concept_names(
        sqlcontext,
        events: DataFrame):
    """
    Description: Add a column of concept names to a Dataframe of snds events
    (loading omop-SNDS concept table)
    :param sqlcontext: a spark sqlContext
    :param events: a Dataframe of snds events
    :return: events_w_infos
    """

    concepts_df = add_ATC2concepts(
        CONCEPTS_PATH, IR_PHA_PATH).loc[
                  :, ['concept_name', 'concept_code', 'domain_id', 'vocabulary_id']]
    concepts = sqlcontext.createDataFrame(concepts_df)
    concepts = concepts.withColumn(
        'concept_code', when(
            func.col('vocabulary_id') == 'SNDS - specialite_ou_nature_activite',
            func.concat_ws('_', func.lit('medical_spe'), func.col('concept_code'))).when(
            func.col('vocabulary_id') == 'SNDS - cip13',
            func.col('concept_code').cast('bigint').cast('string')).otherwise(
            func.col('concept_code')))

    events_w_infos = events.withColumn(
        'value', when(
            func.col('category').isin([
                'non_medical_practitioner_claim', 'medical_practitioner_claim']),
            func.concat_ws('_', func.lit('medical_spe'), func.col('value'))
        ).otherwise(func.col('value'))).alias('a').join(
        concepts.alias('b'),
        func.col('a.value') == func.col('b.concept_code'),
        how='left').fillna('inconnu')

    return events_w_infos


def extract_patient_trajectory(cohort: DataFrame, patient_id: str):
    patient_df = cohort.filter(func.col('patientID') == patient_id).sort(func.col('start').asc())
    if patient_df.count() == 0:
        raise KeyError(
            'Patient {} not in cohort, please enter a patient that is in cohort'.format(patient_id))
    return patient_df.toPandas()


# adaptation necessary for bigoudi
# @todo change for plotly
def plot_patient_trajectory(
        patient_df: pd.DataFrame,
        path2save: str = ''):
    category = 'category'
    category_items = patient_df[category].unique()
    print(category_items)
    # selecting the colors for each unique category in album_name
    palette = brewer['Set3'][len(category_items) + 1]
    # mapping each category with a color of Set2
    colormap = dict(zip(category_items, palette))
    # making a color column based on album_name
    patient_df['color'] = patient_df[category].map(colormap)
    patient_df.loc[:, 'start'] = pd.to_datetime(patient_df['start'])
    source = ColumnDataSource(patient_df.to_dict('list'))
    hover = HoverTool(tooltips=[('Code', '@value'), ('Libellé', '@concept_name'),
                                ('Vocabulaire', '@vocabulary_id')])

    tools = [hover, WheelZoomTool(), PanTool(), BoxZoomTool(), ResetTool(), SaveTool(), TapTool()]
    title = 'Trajectoire patient {}'.format(patient_df['patientID'].values[0])
    p = figure(tools=tools, title=title, plot_width=1400, plot_height=800, toolbar_location='right',
               toolbar_sticky=False, x_axis_type='datetime', y_range=category_items)

    p.scatter(x='start', y=jitter('category', width=0.6, range=p.y_range), source=source, size=10,
              color='color', legend=category, alpha=0.8)

    p.xaxis[0].formatter.days = ['%Y-%m-%d']
    p.x_range.range_padding = 0
    p.ygrid.grid_line_color = None

    if path2save != '':
        output_file('{}.html'.format(path2save), title=title, mode='inline')
        print('Saving schema at {}'.format(path2save))
        save(p)
    show(p)
    return p
