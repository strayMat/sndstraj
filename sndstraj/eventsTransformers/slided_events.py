from sndstraj.eventsTransformers.pmiMatrix import days_to_seconds
from sndstraj.utils.HealthEventsVocab import HealthEventsVocab

import pandas as pd
import os
import logging
from typing import Tuple
from datetime import datetime

from pyspark.sql import DataFrame
from pyspark.sql import Window
import pyspark.sql.functions as func


def build_slided_events_df(
    events: DataFrame,
    save_dir: str = None,
    radius: int = 30,
    delimiter: str = ' ',
    distinct_sboe : bool = True) -> Tuple[pd.DataFrame, HealthEventsVocab]:
    """
    Return a pandas dataframe with for each patient one line by context (of fixed size radius) 
    """
    t0 = datetime.now()
    logging.info("Create vocab with Health vocab")
    health_voca = HealthEventsVocab()
    health_voca.build_vocab_from_events(events, with_deltas=False)
    logging.info("voca contains {} distinct events".format(len(health_voca.value2ix)))
    
    logging.info(f"Create slided sequences in context window of {radius} days")
    radius_in_seconds = days_to_seconds(radius)
    window_start = - radius_in_seconds
    window_end = radius_in_seconds
    w = Window.partitionBy(
        'patientID').orderBy(
        func.col('start').cast('long')).rangeBetween(
        window_start, window_end)

    slided_events = events.withColumn(
        "slided_events",
        func.collect_list(
            func.col("value")
        ).over(w)
    )
    # join array of events as a .csv storable string
    slided_events_joined = slided_events.withColumn(
        'slided_events_join',
        func.array_join('slided_events', delimiter=delimiter)
    )
    # if we slide from one event and the sequence is the same, we don't want to count twice this sequence, hence the distinct
    if distinct_sboe:
        slided_events_joined = slided_events_joined.select('patientID', 'start', 'slided_events_join').distinct()
    slided_events_df = slided_events_joined.select('patientID', 'slided_events_join').toPandas()

    if save_dir:
        if not os.path.isdir(save_dir):
            os.mkdir(save_dir)
        slided_events_df.to_csv(os.path.join(save_dir, "slided_events.csv"), index=False)
        health_voca.save_to_json(os.path.join(save_dir, "health_vocab.json"))
    
    t1 = datetime.now()
    print("Computation ended in {}".format(t1 - t0))
    return slided_events_df, health_voca