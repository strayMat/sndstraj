import json
import os
from operator import attrgetter
from typing import List, Dict

import pyspark.ml.feature as ml_feature
import pyspark.sql.functions as func
from pyspark.sql import DataFrame as psd
from scipy import sparse
from scipy.sparse import csr_matrix, vstack
import numpy as np


def get_spark_bow(
        events: psd,
        output_dir: str = None,
        category_black_list: List = None,
        verbose: int = 1) -> (psd, Dict[str, int]):
    """
    Compute bag of events for patient trajectory as sparse vectors
    :param events: spark event dataframe
    :param output_dir: directory to save the scipy bow and the vocabulary
    :param category_black_list: filter on event category
    :param verbose: print computation details
    :return: pypark dataframe with a column `patient_bow`, vocabulary as label2ix dictionary.
    """
    if category_black_list is None:
        category_black_list = []
    events_filtered = events.filter(
        ~func.col('category').isin(category_black_list)
    )
    events_per_id = events_filtered.groupBy('patientID').agg(
        func.collect_list('value').alias('patient_events')
    )
    codes_vectorizer = ml_feature.CountVectorizer(
        inputCol="patient_events",
        outputCol="patient_bow",
        minDF=0)
    if verbose:
        print('-----Fit count vectorizer model-----')
    codes_vectorizer_model = codes_vectorizer.fit(events_per_id)
    if verbose:
        print('Vocabulary of length {}'.format(len(codes_vectorizer_model.vocabulary)))
    if verbose:
        print('-----Transform events with count vectorizer model-----')
    events_bow = codes_vectorizer_model.transform(events_per_id)
    label2ix = {
        label: i for (label, i) in zip(codes_vectorizer_model.vocabulary,
                                       range(len(codes_vectorizer_model.vocabulary)))
    }
    # save as sparse parquet here (most compressed format)
    if output_dir is not None:
        if not os.path.isdir(output_dir):
            os.makedirs(output_dir)
        events_bow.select('patientID', 'patient_bow').coalesce(500).write.parquet(
            os.path.join(output_dir, 'patient_bow'), mode='overwrite')
        with open(os.path.join(output_dir, 'vocabulary.json'), 'w') as f:
            json.dump(label2ix, f)

    return events_bow.select('patientID', 'patient_bow'), label2ix


def convert_spark_sparse_to_scipy_sparse(
        sparse_psd: psd,
        features_colname: str) -> csr_matrix:
    """
    Convert a column of sparse vectors in a pyspark Dataframe to a scipy sparse matrix.
    Code found here : https://stackoverflow.com/questions/40557577/pyspark-sparse-vectors-to-scipy-sparse-matrix
    Adapted in a one line lambda to be castable into spark.
    :param sparse_psd: pyspark Dataframe with a column of sparse vectors
    :param features_colname: the name of the column of sparse vectors
    :return: scipy matrix of dimension (sparse_pds.count x sparse_vector_size)
    """
    features = sparse_psd.rdd.map(attrgetter(features_colname))
    mats = features.map(
        lambda x: csr_matrix((x.values, x.indices, np.array([0, x.values.size])), tuple((1, x.size)))
    )
    mat = mats.reduce(lambda x, y: vstack([x, y]))
    return mat


def get_scipy_bow(
        events: psd,
        output_dir: str = None,
        category_black_list: List = None,
        verbose: int = 1) -> (csr_matrix, Dict[str, int], Dict[str, int]):
    """
    :param events: spark event dataframe
    :param output_dir: directory to save the scipy bow and the vocabulary
    :param category_black_list: filter on event category
    :param verbose: print computation details
    :return:
    """
    spark_bow, label2ix = get_spark_bow(
        events=events,
        output_dir=None,
        category_black_list=category_black_list,
        verbose=verbose
    )
    scipy_bow = convert_spark_sparse_to_scipy_sparse(
        sparse_psd=spark_bow,
        features_colname='patient_bow'
    )
    pIDs = spark_bow.select('patientID').toPandas().values.flatten()
    patientID2ix = {pID: i for (pID, i) in zip(pIDs, range(len(pIDs)))}
    if output_dir is not None:
        if not os.path.isdir(output_dir):
            os.makedirs(output_dir)
        sparse.save_npz(os.path.join(output_dir, 'sparse_bow.npz'), scipy_bow)
        with open(os.path.join(output_dir, 'vocabulary.json'), 'w') as f:
            json.dump(label2ix, f)
        with open(os.path.join(output_dir, 'patientID2ix.json'), 'w') as f:
            json.dump(patientID2ix, f)
    return scipy_bow, label2ix, patientID2ix
