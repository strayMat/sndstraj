import numpy as np
from torch.utils import data


class MedicalCodesDataset(data.Dataset):
    #  @todo dump this into  NextEventLoader
    """Characterizes a dataset for PyTorch, custom implementation to deal with sequence data"""

    def __init__(self, sequences, list_ids, max_length, padding_ix, condition_task_weight=0.5):
        """Initialization"""
        assert (0 <= condition_task_weight) and (condition_task_weight <= 1), \
            print("Enter correct weight for the condition task weight: alpha should be in [0, 1]")
        self.sequences = sequences
        self.list_IDs = list_ids
        # self.labels = labels
        self.max_length = max_length
        self.padding_ix = padding_ix
        self.condition_task_weight = condition_task_weight

    def __len__(self):
        """Denotes the total number of samples"""
        return len(self.list_IDs)

    def __getitem__(self, index):
        """Generates one sample of data"""
        # Select sample
        patient_id = self.list_IDs[index]
        # Load data and get label
        patient_sequence = self.sequences.loc[
            self.sequences.index == patient_id, "events_sequences"].values[0]
        patient_sequence = np.array([int(i) for i in patient_sequence.strip().split(" ")])

        x = patient_sequence[:-1]
        y = patient_sequence[1:]
        condition = 0
        if self.condition_task_weight > 0:
            condition = self.sequences.loc[self.sequences.index == patient_id, "label"].values[0]

        seq_length = len(x)
        if seq_length > self.max_length:
            x = x[-self.max_length:]
            y = y[-self.max_length:]
            seq_length = 10
        else:
            x = np.concatenate((x,
                                np.repeat(self.padding_ix, self.max_length - len(x))))
            y = np.concatenate((y,
                                np.repeat(self.padding_ix, self.max_length - len(y))))

        return x, y, seq_length, condition
