import os
import random
from typing import List, Dict

import numpy as np
import pandas as pd
import pyspark.sql.functions as func
import torch
from itertools import chain
from pyspark.sql import DataFrame
from pyspark.sql import Window
from pyspark.sql.functions import col, create_map, lit
from time import time
from torch.utils import data

from sndstraj.utils.HealthEventsVocab import HealthEventsVocab

SEP = '<sep>'


# SEP = ' '

def delta_from_events(
        events_df: DataFrame,
        output_dir: str,
        output_format: str = 'pandas',
        with_deltas: bool = True,
        target_patients: DataFrame = None,
        min_nb_events: int = 3,
        categories_blacklist: List[str] = None,
        verbose: int = 0) -> DataFrame:
    """
    Use spark sql context to preprocess data : build dictionary, sort events by patient and
    dates of visits, sort the sequence.
    :param events_df:
    :param output_dir:
    :param output_format: available (pandas, parquet)
    :param with_deltas:
    :param target_patients:
    :param min_nb_events:
    :param categories_blacklist:
    :param verbose:
    :return:
    """
    t0 = time()
    if categories_blacklist is None:
        categories_blacklist = []
    if with_deltas:
        features = ['values', 'deltas']
    else:
        features = ['values']
    events_df = events_df.filter(~func.col("category").isin(categories_blacklist))

    if os.path.isdir(output_dir):
        print("WARN: Output directory already existing, overriding files...")
    else:
        os.makedirs(output_dir)

    if verbose:
        print("-----Sorting events-----")
    w = Window.orderBy('patientID', 'start').partitionBy('patientID')
    sorted_events = events_df.withColumn(
        "previous_start", func.lag('start').over(w)).withColumn(
        "delta", ((func.col("start").cast('long') -
                   func.col("previous_start").cast('long')) / (3600 * 24)).cast('integer')).fillna(
        -1, subset=['delta'])  # first delta of the sequence is -1
    # require one pass on the data (not too costly in pyspark)
    vocab = HealthEventsVocab(categories_blacklist=categories_blacklist)
    vocab.build_vocab_from_events(
        events=sorted_events,
        with_deltas=with_deltas,
        verbose=verbose)

    path2vocab = os.path.join(output_dir, "vocabulary.json")
    if verbose:
        print("Saving vocabulary in {}".format(path2vocab))
    vocab.save_to_json(path2vocab=path2vocab)

    mapping_expr = create_map([lit(x) for x in chain(*vocab.value2ix.items())])

    if verbose:
        print("-----Mapping events to vocabulary indices-----")
    sorted_events = sorted_events.withColumn(
        "value", mapping_expr.getItem(col("value")).cast("string"))
    # aggregate by patient and collect lists of strings
    aggregated_events = sorted_events.groupBy("patientID").agg(
        func.collect_list(func.col("value")).alias("values"),
        func.collect_list(func.col("delta").cast('string')).alias("deltas"),
        func.count(func.col("value")).alias("nb_events")
    ).filter(col("nb_events") >= min_nb_events)
    if not with_deltas:
        aggregated_events = aggregated_events.drop('delta')
    if verbose:
        print("-----Converting to pandas-----")
    # case target patients exist.
    if target_patients is not None:
        target_label = target_patients.select(
            'patientID'
        ).distinct().withColumn('label', func.lit(1))

        events_w_labels = aggregated_events.join(
            target_label,
            'patientID',
            'left'
        ).fillna(0)
        pd_df = events_w_labels.select(['patientID', 'label'] + features)
    else:
        pd_df = aggregated_events.select(['patientID'] + features)
    # two strategies depending on the desired output
    if output_format == 'pandas':
        pandas_df = pd_df.toPandas()
        pandas_df = pandas_df.set_index('patientID')
        pandas_df['values'] = pandas_df['values'].map(lambda x: SEP.join(x))
        if with_deltas:
            pandas_df['deltas'] = pandas_df['deltas'].map(lambda x: SEP.join(x))
        pandas_df.to_csv(os.path.join(output_dir, 'patients_sequences.csv'), index=True)
        result_df = pandas_df
    elif output_format == 'parquet':
        pd_df.orderBy(func.col("patientID")).write.partitionBy("patientID").parquet(
            os.path.join(output_dir, 'patients_sequences.parquet')
        )
        result_df = pd_df
    if verbose:
        print("-----Preprocessing ended well-----")
        print("INFO: Time elapsed: {}".format(time() - t0))
    return result_df


def get_values_deltas(
        sequences: pd.DataFrame,
        patient_id: str) -> (List[str], List[str]):
    """
    Extract values and delta for a given patient ID from the preprocessed sequences
    :param sequences:
    :param patient_id:
    :return:
    """
    patient_sequence = sequences.loc[patient_id]
    x_values = [i for i in patient_sequence["values"].strip().split(SEP)]
    x_deltas = [i for i in patient_sequence["deltas"].strip().split(SEP)]
    return x_values, x_deltas


class NextEventLoader(data.Dataset):
    # todo : try to use a sparkContext for the __get_item__ method and time it.
    """Characterizes a dataset for PyTorch, 
    custom implementation to deal with timed sequential data
    usage with a torch.utils.data.Dataloader : https://pytorch.org/docs/stable/data.html"""

    def __init__(
            self,
            sequences: pd.DataFrame,
            target_ids: List,
            max_length: int,
            sos_ix: int,
            eos_ix: int,
            padding_ix: int,
            condition_task_weight: float = 0.5,
            on_memory: bool = True):
        """
        :param sequences: delta sequences pandas data frame, format : {index: patientID,
        :param target_ids: ids that should be included in the generator
        :param max_length:
        :param padding_ix:
        """
        assert (0 <= condition_task_weight) and (condition_task_weight <= 1), \
            print("Enter correct weight for the condition task weitght : alpha should be in [0, 1]")
        if on_memory:
            self.sequences = sequences
        else:
            # todo find something for huge dataset
            #  (might not be necessary depending of the size of one year of snds in sequence format)
            raise NotImplementedError
        self.target_IDs = target_ids
        self.max_length = max_length
        self.padding_ix = padding_ix
        self.sos_ix = sos_ix
        self.eos_ix = eos_ix
        self.condition_task_weight = condition_task_weight

    def __len__(self):
        """Denotes the total number of samples"""
        return len(self.target_IDs)

    def __getitem__(self, index):
        """Generates one sample of data"""
        # Select sample
        patient_id = self.target_IDs[index]
        # Load data and get label
        patient_values, patient_deltas = get_values_deltas(self.sequences, patient_id)

        x_values = np.concatenate([self.sos_ix] + patient_values)
        x_deltas = np.concatenate([self.sos_ix] + patient_deltas)
        y_values = np.concatenate(patient_values + [self.eos_ix])
        y_deltas = np.concatenate(patient_deltas + [self.eos_ix])

        condition = 0
        if self.condition_task_weight > 0:
            condition = self.sequences.loc[patient_id]["label"].values[0]

        seq_length = len(x_values)
        if seq_length > self.max_length:
            x_values = x_values[-self.max_length:]
            x_deltas = x_deltas[-self.max_length:]
            y_values = y_values[-self.max_length:]
            y_deltas = y_deltas[-self.max_length:]
        else:
            x_values = np.concatenate((x_values,
                                       np.repeat(self.padding_ix, self.max_length - seq_length)))
            x_deltas = np.concatenate((x_deltas,
                                       np.repeat(self.padding_ix, self.max_length - seq_length)))
            y_values = np.concatenate((y_values,
                                       np.repeat(self.padding_ix, self.max_length - seq_length)))
            y_deltas = np.concatenate((y_deltas,
                                       np.repeat(self.padding_ix, self.max_length - seq_length)))

        return x_values, x_deltas, y_values, y_deltas, condition


class MaskedEventsLoader(data.Dataset):
    """
    Custom dataloader implementation to perform masked language model with timed sequential data
    """

    def __init__(
            self,
            sequences: pd.DataFrame,
            target_ids: List,
            max_length: int,
            vocab: HealthEventsVocab):
        self.sequences = sequences
        self.target_IDs = target_ids
        self.max_length = max_length
        self.vocab = vocab

    def __len__(self):
        """Denotes the total number of samples"""
        return len(self.target_IDs)

    def __getitem__(self, index):
        # Select sample
        patient_id = self.target_IDs[index]
        # Load data and get label
        patient_values, patient_deltas = get_values_deltas(
            self.sequences,
            patient_id)

        # mlm on values
        x_values, y_values, x_deltas, y_deltas = self.get_mlm(patient_values, patient_deltas)
        output = {'x_values': x_values, 'y_values': y_values,
                  'x_deltas': x_deltas, 'y_deltas': y_deltas}
        seq_length = len(x_values)
        if seq_length > self.max_length:
            output = {key: seq[:self.max_length] for (key, seq) in output.items()}
        else:
            padding = list(np.repeat(self.vocab.pad_ix, self.max_length - seq_length))
            output = {key: seq + padding for (key, seq) in output.items()}
        ## to adapt for sequence of care
        # segment_label = ([1 for _ in range(len(t1))] + [2 for _ in range(len(t2))])[:self.seq_len]
        output = {key: torch.tensor(seq) for (key, seq) in output.items()}

        return list(output.values())

    def get_mlm(self, tokens: List[str], deltas: List[str]):
        output_tokens = []
        output_deltas = []

        for i, (token, delta) in enumerate(zip(tokens, deltas)):
            prob = random.random()
            if prob < 0.15:
                prob /= 0.15

                # 80% randomly change token to mask token
                if prob < 0.8:
                    tokens[i] = self.vocab.mask_ix
                    deltas[i] = self.vocab.mask_ix
                # 10% randomly change token to random token
                elif prob < 0.9:
                    tokens[i] = random.randrange(len(self.vocab.value2ix))
                    deltas[i] = random.randrange(len(self.vocab.delta2ix))
                # 10% randomly change token to current token (donothing)
                else:
                    tokens[i] = self.vocab.value2ix[token]
                    deltas[i] = self.vocab.delta2ix[delta]

                output_tokens.append(self.vocab.value2ix[token])
                output_deltas.append(self.vocab.delta2ix[delta])

            else:
                # donothing but convert string to ix
                tokens[i] = self.vocab.value2ix[
                    token]  # tokens[i] = self.vocab.stoi.get(token, self.vocab.unk_index)
                deltas[i] = self.vocab.delta2ix[delta]
                # adding padding ix to output (will be masked for the loss=
                output_tokens.append(self.vocab.pad_ix)
                output_deltas.append(self.vocab.pad_ix)

        tokens = [self.vocab.sos_ix] + tokens + [self.vocab.eos_ix]
        output_tokens = [self.vocab.pad_ix] + output_tokens + [self.vocab.pad_ix]
        deltas = [self.vocab.sos_ix] + deltas + [self.vocab.eos_ix]
        output_deltas = [self.vocab.pad_ix] + output_deltas + [self.vocab.pad_ix]
        return tokens, output_tokens, deltas, output_deltas
