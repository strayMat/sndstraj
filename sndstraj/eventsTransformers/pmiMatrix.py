import json
import os
from datetime import datetime
from typing import List, Tuple, Dict

import numpy as np
import pyspark.ml.feature as ml_feature
import pyspark.sql.functions as func
from itertools import chain
from pyspark.mllib.linalg import DenseVector
from pyspark.sql import DataFrame
from pyspark.sql import Window


def days_to_seconds(days: int) -> int:
    return days * 86400


def join_lists(list_2d: List[List[str]]) -> List[str]:
    return list(chain(*list_2d))


def build_cooccurrence_matrix(
        events: DataFrame,
        output_dir: str = None,
        radius_in_days: int = 30,
        window_orientation: str = 'centered',
        matrix_type: str = 'numpy',
        verbose: int = 1) -> Tuple[np.array, np.array, Dict]:
    """
    Description: Leverage spark logic to compute cooccurrence matrix for an event table,
    :param events: event dataframe
    :param output_dir:
    :param radius_in_days: size of the context window in days, this is to be thought as the radius
    around the central word of reference. Depending of the orientation of the window, the size of
    window will be 2 * radius_in_days if centered
    :param window_orientation: choose position of the reference word in the context window:
        ['centered', 'future'], this changes the focus of the co-occurrence matrix.
        NB : The future configuration leads to an asymetric cooccurrence matrix.
    :param matrix_type:
    :param verbose:
    :return:
        - cooccurrence_matrix, matrix of cooccurrence of size VxV  where V is the vocabulary size
         with M[i, j] = #|i and j occurs in a window of size window_in_days|
        - item_count, array of size V with the number of single occurrence of each word
        - label2ix, dictionary of size V giving the correspondence between a word and its index in
        the matrix
    """
    # convert days into timesecond
    radius_in_seconds = days_to_seconds(radius_in_days)
    # chose window orientation
    if window_orientation == 'centered':
        window_start = - radius_in_seconds
        window_end = radius_in_seconds
    elif window_orientation == 'future':
        window_start = 0
        window_end = radius_in_seconds
    else:
        raise ValueError("Choose window_orientation in ['centered', 'future']")
    # little hack of the rangeBetween function
    w = Window.partitionBy('patientID').orderBy(
        func.col('start').cast('long')).rangeBetween(
        window_start, window_end)
    events_in_window = events.withColumn(
        'cooccurrence', func.collect_list(func.col('value')).over(w))

    codes_vectorizer = ml_feature.CountVectorizer(
        inputCol="cooccurrence",
        outputCol="cooccurrence_ohe",
        minDF=0)

    if verbose:
        print('-----Fit count vectorizer model-----')
    codes_vectorizer_model = codes_vectorizer.fit(events_in_window)
    if verbose:
        print('Vocabulary of length {}'.format(len(codes_vectorizer_model.vocabulary)))
    if verbose:
        print('-----Transform events with count vectorizer model-----')
    events_ohe = codes_vectorizer_model.transform(events_in_window)
    label2ix = {label: i for (label, i) in zip(codes_vectorizer_model.vocabulary,
                                               range(len(codes_vectorizer_model.vocabulary)))}
    # adding ix for future reduced key
    mapping_expr = func.create_map([func.lit(x) for x in chain(*label2ix.items())])
    events_ohe_with_ix = events_ohe.withColumn(
        'ix', mapping_expr.getItem(func.col("value")).cast("int"))
    # sort by item ix and get back raw counts (suppres
    # s item ignored by count vectorizer)
    item_count = np.array(
        events_ohe_with_ix.groupBy('ix').count().filter(
            ~func.col('ix').isNull()).sort(func.col('ix')).drop('ix').collect()).reshape(
        -1)

    # aggregate one line per item and sort by item index
    events_ohe_grouped = events_ohe_with_ix.select('ix', 'cooccurrence_ohe').rdd.mapValues(
        lambda v: v.toArray()).reduceByKey(
        lambda x, y: x + y).mapValues(
        lambda x: DenseVector(x)).toDF(["ix", "cooccurrence_ohe"])
    # remove excluded code from count_vectorizer and sort by ix
    events_ohe_grouped_sorted = events_ohe_grouped.filter(
        ~func.col('ix').isNull()).sort(func.col('ix').asc())
    # collect and reshape the cooccurrence matrix
    if matrix_type == 'numpy':
        if verbose:
            print('-----Collect co-occurrence matrix as numpy-----')
        x_3d = np.array(events_ohe_grouped_sorted.select('cooccurrence_ohe').collect())
        rows, idx, vocab_size = x_3d.shape
        cooccurrence_matrix = x_3d.reshape(rows, rows) - np.diag(item_count)
        if output_dir is not None:
            if not os.path.isdir(output_dir):
                os.makedirs(output_dir)
            with open(os.path.join(output_dir, 'vocabulary.json'), 'w') as f:
                json.dump(label2ix, f)
            np.save(os.path.join(output_dir, 'cooccurrence_matrix.npy'), cooccurrence_matrix)
            np.save(os.path.join(output_dir, 'codes_counts.npy'), item_count)
    else:
        raise NotImplementedError('Collect spark matrix not implemented')
    #     if verbose:
    #         print('-----Compute co-occurrence matrix as dense spark matrix-----')
    #     sparse_to_array_udf = func.udf(sparse_to_array, T.ArrayType(T.FloatType()))
    #     dense_cooccurrence = events_ohe_grouped_sorted.withColumn(
    #         'dense_cooccurrences', sparse_to_array_udf('cooccurrence_ohe')).select(
    #         'dense_cooccurrences')
    #     cooccurrence_matrix = RowMatrix(dense_cooccurrence.rdd.map(lambda x: x[0]))

    return cooccurrence_matrix, item_count, label2ix


def sparse_to_array(v):
    """
    Description: udf to transform to dense vector spark sparse vector
    :param v:
    :return:
    """
    v = DenseVector(v)
    new_array = list([float(x) for x in v])
    return new_array


def build_pmi(
        cooccurrence_matrix: np.array,
        item_count: np.array,
        smoothing_factor: float = 0.75) -> np.array:
    """
    Description: Build the pmi matrix from the cooccurrence matrix M:
    $$pmi = log[\frac{p(w,c)}{p(w)p(c)}]$$
    # Take the raw item count as the Z denominator
    # We might try some large definition of context where we throws up from the denominator
    # all self-cooccurrence (ie windows of size 1 with an item alone)
    # Our lines/columns do not sum to 1 because we exclude the column where there are counts of
    # the line item co-occurring with the same occurrence of itself.
    # The diagonal corresponds only to co-occurrences of the item with a different occurrence of
    # itself.
    :param cooccurrence_matrix:
    :param item_count:
    :param smoothing_factor:
    :return:
    """

    Z = item_count.sum()
    cooccurrence_ratio = cooccurrence_matrix / Z
    item_ratio = (item_count / Z) ** smoothing_factor
    inverse_item_ratio = 1 / item_ratio
    pmi_matrix = np.log(cooccurrence_ratio * np.outer(inverse_item_ratio, inverse_item_ratio))

    return pmi_matrix


def build_ppmi(pmi: np.array, k: int = 1) -> np.array:
    """
    Description: Build shifted pmi
    :param pmi:
    :param k:
    :return:
    """
    ppmi = pmi - np.log(k)
    ppmi[ppmi <= 0] = 0

    return ppmi


def build_embeddings(ppmi: np.array, d: int, window_orientation='centered'):
    """
    TODO should use pyspark.mlib if the matrix is too big for numpy
    Description: perform symetric singular value reconstruction
    :param window_orientation: choose position of the reference word in the context window:
        ['centered', 'future'], this changes the focus of the co-occurrence matrix.
        NB : The future configuration leads to an asymetric cooccurrence matrix.
    :param ppmi:
    :param d:
    :return:
    """
    if window_orientation == 'centered':
        ppmi = ppmi
    elif window_orientation == 'future':
        # symetrization of the cooccurrence matrix
        ppmi = (ppmi + np.transpose(ppmi)) / 2
    else:
        raise ValueError("Choose window_orientation in ['centered', 'future']")
    if d > ppmi.shape[0]:
        raise Exception('Continuous vector dimension should be lower than vocabulary size!')
    U, S, V = np.linalg.svd(ppmi)
    U_d = U[:, :d]
    V_d = V[:, :d]
    S_d = S[:d]
    embeddings = U_d.dot(np.diag(np.sqrt(S_d))) + V_d.dot(np.diag(np.sqrt(S_d)))

    return embeddings


def snds2vec(
        cooccurrence_matrix: np.array,
        item_count: np.array,
        window_orientation: str = 'centered',
        d: int = 300,
        smoothing_factor: float = 0.75,
        k: int = 1,
        verbose=1):
    """
    Description: Wrapper around build_pmi, build_ppmi and build_embeddings to create snds2vec from
    the cooccurrence matrix.
    :param window_orientation: choose position of the reference word in the context window:
        ['centered', 'future'], this changes the focus of the co-occurrence matrix.
        NB : The future configuration leads to an asymetric cooccurrence matrix.
    :param cooccurrence_matrix:
    :param item_count:
    :param d:
    :param smoothing_factor:
    :param k:
    :param verbose:
    :return:
    """
    if window_orientation not in ['future', 'centered']:
        raise ValueError("Choose window_orientation in ['centered', 'future']")
    t0 = datetime.now()
    if verbose:
        print('Shape of the co-occurrence matrix: '.format(cooccurrence_matrix.shape))
        print('---Build PMI with smoothing factor {}---'.format(smoothing_factor))
    pmi = build_pmi(
        cooccurrence_matrix=cooccurrence_matrix,
        item_count=item_count,
        smoothing_factor=smoothing_factor)
    if verbose:
        print('---Build PPMI with shift factor {}---'.format(k))
    ppmi = build_ppmi(pmi=pmi, k=k)
    if verbose:
        print('---PPMI factorization with SVD---')
    embeddings = build_embeddings(ppmi=ppmi, d=d, window_orientation=window_orientation)
    t1 = datetime.now()
    if verbose:
        print(
            'Embeddings of dimension {} created from the co-occurrence matrix in {}'.format(
                d, t1 - t0))

    return embeddings
