import copy
import json
from typing import Dict

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable


# Implementation of the self-attention encoder/decoder logics from the paper :
# "Attention is all you need", code found here :
# http://nlp.seas.harvard.edu/2018/04/03/attention.html


def clones(module: nn.Module, N: int):
    """Produce N identical layers."""
    return nn.ModuleList([copy.deepcopy(module) for _ in range(N)])


class Encoder(nn.Module):
    """Core encoder is stack of N layers"""

    def __init__(self, layer: nn.modules, N: int):
        super(Encoder, self).__init__()
        self.layers = clones(layer, N)
        self.norm = LayerNorm(layer.size)

    def forward(self, x, mask):
        """Pass the input and mask to each layer"""
        for layer in self.layers:
            x = layer(x, mask)
        return self.norm(x)


class LayerNorm(nn.Module):
    """Using layer normalization (https://arxiv.org/abs/1607.06450)"""

    def __init__(self, features, eps=1e-6):
        super(LayerNorm, self).__init__()
        self.a_2 = nn.Parameter(torch.ones(features))
        self.b_2 = nn.Parameter(torch.zeros(features))
        self.eps = eps

    def forward(self, x):
        mean = x.mean(-1, keepdim=True)
        std = x.std(-1, keepdim=True)
        return self.a_2 * (x - mean) / (std + self.eps) + self.b_2


class SublayerConnection(nn.Module):
    """A residual connection (https://arxiv.org/abs/1512.03385) followed by a layer norm.
    Note for code simplicity the norm is first as opposed to last."""

    def __init__(self, size, dropout):
        super(SublayerConnection, self).__init__()
        self.norm = LayerNorm(size)
        self.dropout = nn.Dropout(dropout)

    def forward(self, x, sublayer):
        return x + self.dropout(sublayer(self.norm(x)))


class EncoderLayer(nn.Module):
    """Encoder is made of self attention and feed forward layer"""

    def __init__(self, size, self_attn, feed_forward, dropout):
        super(EncoderLayer, self).__init__()
        self.self_attn = self_attn
        self.feed_forward = feed_forward
        self.sublayer = clones(SublayerConnection(size, dropout), 2)
        self.size = size

    def forward(self, x, mask):
        x = self.sublayer[0](x, lambda z: self.self_attn(z, z, z, mask))
        return self.sublayer[1](x, self.feed_forward)


def attention(
        query: torch.tensor,
        key: torch.tensor,
        value: torch.tensor,
        mask: torch.tensor = None,
        dropout=None) -> (torch.tensor, torch.tensor):
    """
    Compute scaled dot product attention
    :param query:
    :param key:
    :param value:
    :param mask:
    :param dropout:
    :return:
    """
    d_k = query.size(-1)
    scores = torch.matmul(query, key.transpose(-2, -1)) / torch.sqrt(d_k)
    if mask is not None:
        scores = scores.masked_fill(mask == 0, -1e9)
    p_att = F.softmax(scores, dim=-1)
    if dropout is not None:
        p_att = dropout(p_att)
    return torch.matmul(p_att, value), p_att


def subsequent_mask(size):
    """Mask out subsequent positions."""
    attn_shape = (1, size, size)
    subsequent_mask = np.triu(np.ones(attn_shape), k=1).astype('uint8')
    return torch.from_numpy(subsequent_mask) == 0


class MultiHeadedAttention(nn.Module):
    def __init__(self, h: int, d_model: int, dropout: float = 0.1):
        super(MultiHeadedAttention, self).__init__()
        assert d_model % h == 0
        self.d_k = d_model / h
        self.h = h
        self.linears = clones(nn.Linear(d_model, d_model), 4)
        self.attn = None
        self.dropout = nn.Dropout(p=dropout)

    def forward(self, query, key, value, mask=None):
        if mask is not None:
            mask = mask.unsqueeze(1)
        nbatches = query.size(0)

        # Do linear projections in batch (d_model -> h x d_k)
        query, key, value = \
            [l(x).view(nbatches, -1, self.h, self.d_k).transpose(1, 2) for l, x in
             zip(self.linears, (query, key, value))]

        #  apply self attention in batch
        x, self.attn = attention(query, key, value, mask=mask, dropout=self.dropout)
        x = x.transpose(1, 2).contiguous().view(nbatches, -1, self.h * self.d_k)
        return self.linears[-1](x)


class PositionFeedForward(nn.Module):
    """Ffn applied identically to each position"""

    def __init__(self, d_model: int, d_ff: int, dropout: float = 0.1):
        super(PositionFeedForward, self).__init__()
        self.w_1 = nn.Linear(d_model, d_ff)
        self.w_2 = nn.Linear(d_ff, d_model)
        self.dropout = nn.Dropout(dropout)

    def forward(self, x):
        return self.w_2(self.dropout(F.relu(self.w_1(x))))


class PositionalEncoding(nn.Module):
    #  todo Modify this to take timesteps instead of position
    """Position encoding function :
    PE(pos, 2i) = sin(pos/10000^2i/d_model) and PE(pos, 2i+1) = cos(pos/10000^2i/d_model)"""

    def __init__(self, d_model, dropout, max_len=5000):
        super(PositionalEncoding, self).__init__()
        self.dropout = nn.Dropout(dropout)
        pe = torch.zeros(max_len, d_model)
        position = torch.arange(0., max_len).unsqueeze(1)
        div_term = torch.exp(torch.arange(0., d_model, 2) * -(np.log(1000.0) / d_model))
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)
        pe.unsqueeze(0)
        self.register_buffer('pe', pe)

    ### ???
    def forward(self, x):
        x = x + Variable(
            self.pe[:, :x.size(1)],
            requires_grad=False)

        return self.dropout(x)


class Embeddings(nn.Module):
    #  todo adapt to take into account learned embeddings
    def __init__(
            self,
            d_model,
            vocab: Dict,
            pretrained_path: str = None,
            trainable: bool = True,
            verbose: int = 1):
        super(Embeddings, self).__init__()
        self.d_model = d_model
        self.pretrained_path = pretrained_path
        self.trainable = trainable
        if self.pretrained_path is not None:
            with open(self.pretrained_embeddings_path, 'r') as f:
                pretrained_dict = json.load(f)
            pretrained_dim = len(list(pretrained_dict.values())[0])
            assert self.d_model == pretrained_dim, \
                'Pretrained Embeddings should be of the same dimension as model embedding dimension, catch {} but expected {}'.format(
                    pretrained_dim, self.d_model)
            weights_matrix = torch.zeros((len(list(pretrained_dict.values())), self.d_model))
            nb_replaced_embeddings = 0
            for code, ix in self.vocab.items():
                if code in pretrained_dict.keys():
                    weights_matrix[ix] = pretrained_dict[code]
                    nb_replaced_embeddings += 1
                else:
                    # todo : check for the pertinence of this initialization
                    weights_matrix[ix] = torch.random.normal(scale=0.6, size=(self.d_model,))
            self.lut = nn.Embedding.from_pretrained(torch.Tensor(weights_matrix))
            if verbose:
                print('{}/{} codes in voca match pretrained embeddings'.format(
                    nb_replaced_embeddings, len(self.vocab)))
        #  if no pretrained, random initialization
        else:
            self.lut = nn.Embedding(len(vocab), d_model)
        # trainable or frozen embeddings ?
        self.lut.weight.requires_grad = self.trainable
        if verbose:
            print("Trainable embeddings : {}".format(self.trainable))

    def forward(self, x):
        return self.lut(x) * np.sqrt(self.d_model)


class MaskedLMHead(nn.Module):
    """Prediction head for MLM task (pretraining)"""
    def __init__(self, d_model: int, vocab_len: int):
        super(MaskedLMHead, self).__init__()
        self.proj = nn.Linear(d_model, vocab_len, bias=False)
        self.bias = nn.Parameter(torch.zeros(vocab_len))

    def forward(self, x):
        return F.log_softmax(self.proj(x) + self.bias, dim=-1)


class TransfoMed(nn.Module):
    def __init__(self, encoder, src_embed, prediction_head):
        super(TransfoMed, self).__init__()
        self.encoder = encoder
        self.src_embed = src_embed
        self.prediction_head = prediction_head

    def forward(self, src, src_mask):
        self.encoder(self.src_embed(src), src_mask)


def make_model(
        src_vocab: Dict,
        N: int = 6,
        d_model: int = 512,
        d_ff: int = 2048,
        h: int = 8,
        dropout: float = 0.1):
    c = copy.deepcopy
    attn = MultiHeadedAttention(h, d_model)
    ff = PositionFeedForward(d_model, d_ff, dropout)
    position = PositionalEncoding(d_model, dropout)
    prediction_head = MaskedLMHead(d_model, len(src_vocab))
    model = TransfoMed(
        encoder=Encoder(EncoderLayer(d_model, c(attn), c(ff), dropout), N),
        src_embed=nn.Sequential(Embeddings(d_model, src_vocab), c(position)),
        prediction_head=prediction_head
    )
    for p in model.parameters():
        if p.dim() > 1:
            nn.init.xavier_uniform_(p)
    return model


class Batch:
    """Create batch and add mask for padding and future words"""

    def __init__(self, src, vocab: Dict, pad=0, prop:float=0.15):
        self.src = src
        self.src_mask = (src != pad).unsqueeze(-2)
        self.tgt_mask = self.make_mlm_mask(self.src, vocab, pad=pad, prop=prop)
    @staticmethod
    def make_mlm_mask(src: torch.Tensor, vocab: Dict, pad, prop: float = 0.15):
        "Create a mask to hide padding and mask codes"
        tgt_mask = (src != pad).unsqueeze(-2)
        tgt_mask = tgt_mask & Variable(
            subsequent_mask(src.size(-1)).type_as(tgt_mask.data))
        return tgt_mask

# class McAttention(nn.Module):
#     def __init__(
#             self,
#
#             pad_token,
#             device,
#             max_len=10,
#             batch_size=32,
#             pretrained_embeddings_path: str = None,
#             trainable_embeddings: bool = True,
#             condition_task_weight=0.5,
#             prediction_layer='mean_pooling',
#             dataloader_nbworkers: int = 8,
#             verbose: int = 1):
#
#         super(McAttention, self).__init__()
#         # prediction_layer_possibilities = ['mean_pooling', 'max_pooling', 'last_hidden']
#         # assert prediction_layer in prediction_layer_possibilities, \
#         #    'prediction_layer should be in {}'.format(prediction_layer_possibilities)
#
#         # sep stands for Sequential Event Prediction (predict next events)
#         assert (0 <= condition_task_weight) and (condition_task_weight <= 1), \
#             print("Enter correct weight for the condition task weight: should be in [0, 1]")
#
#         self.device = device
#         self.vocab = vocab
#         self.max_len = max_len
#         self.vocabulary_size = len(vocab)
#         self.padding_idx = self.vocab[pad_token]
#         self.batch_size = batch_size
#         self.pretrained_embeddings_path = pretrained_embeddings_path
#         self.trainable_embeddings = trainable_embeddings
#         self.embedding_dim = embedding_dim
#         self.hidden_dim = hidden_dim
#         self.dropout = dropout
#         self.condition_task_weight = condition_task_weight
#         self.prediction_layer = prediction_layer
#         self.dataloader_nbworkers = dataloader_nbworkers
#
#         if self.condition_task_weight > 0:
#             self.hidden2binary = nn.Linear(self.hidden_dim, 2)
#             self.condition_log_softmax = nn.LogSoftmax(dim=1)
#
#     def loss(self, y_pred, y, pred_condition=None, condition=None):
#         """A loss function that deals with padded items and multi-task learning"""
#
#         y = y.contiguous().view(-1)
#         # flatten all predictions
#         y_pred = y_pred.contiguous().view(-1, self.vocabulary_size)
#         # create a mask by filtering out all tokens that ARE NOT the padding token
#         tag_pad_token = self.padding_idx
#         mask = (y != tag_pad_token).float()
#         # count how many tokens we have
#         nb_tokens = int(torch.sum(mask).item())
#
#         # weighted Cross-entropy loss with the mask  as weights (ignore <PAD> tokens)
#         y_pred = y_pred[range(y_pred.shape[0]), y] * mask
#         ce_loss = -torch.sum(y_pred) / nb_tokens
#
#         # cross entropy loss for condition
#         condition_loss = nn.NLLLoss()
#         #  choose supervision depending on the task
#         if self.condition_task_weight > 0:
#             condition = condition.contiguous().view(-1)
#             pred_condition = pred_condition.contiguous().view(-1, 2)
#             return (
#                            1 - self.condition_task_weight) * ce_loss + self.condition_task_weight * condition_loss(
#                 pred_condition, condition)
#         elif self.condition_task_weight == 1:
#             condition = condition.contiguous().view(-1)
#             pred_condition = pred_condition.contiguous().view(-1, 2)
#             return condition_loss(pred_condition, condition)
#         elif self.condition_task_weight == 0:
#             return ce_loss
#
#     def train_model(
#             self,
#             sequences,
#             train_ids,
#             validation_ids,
#             optimizer,
#             nb_epochs
#     ):
#
#         dataloader_params = {
#             'batch_size': self.batch_size,
#             'shuffle': True,
#             'num_workers': self.dataloader_nbworkers,
#             'drop_last': True
#         }
#
#         # optimizer = optim.SGD(model.parameters(), lr=0.1)
#
#         #  Build data generators
#         train_set = MedicalCodesDataset(sequences,
#                                         train_ids,
#                                         self.max_len,
#                                         self.padding_idx,
#                                         condition_task_weight=self.condition_task_weight)
#         train_generator = data.DataLoader(train_set, **dataloader_params)
#
#         validation_set = MedicalCodesDataset(sequences,
#                                              validation_ids,
#                                              self.max_len,
#                                              self.padding_idx,
#                                              condition_task_weight=self.condition_task_weight)
#         validation_generator = data.DataLoader(validation_set, **dataloader_params)
#
#         # initialise history
#         train_loss_history = []
#         validation_loss_history = []
#         train_total_loss = 0.
#         dev_total_loss = 0.
#
#         self.train()
#         start_time = time.time()
#
#         # TODO: let epoch be a parameter of the network
#         epoch = 0
#         for epoch in range(nb_epochs):
#             epoch_start = time.time()
#             train_epoch_loss = 0.
#             # setup the model in train mode
#             self.train()
#             for i, (local_batch, local_labels, seq_lengths, local_conditions) in enumerate(
#                     train_generator):
#                 optimizer.zero_grad()
#                 self.zero_grad()
#                 # order batch and send to device
#                 decreasing_order = (-seq_lengths).argsort()
#                 seq_lengths = seq_lengths[decreasing_order]
#                 batch_max_len = max(seq_lengths)
#                 x = local_batch[decreasing_order][:, :batch_max_len].long().to(self.device)
#                 y = local_labels[decreasing_order][:, :batch_max_len].long().to(self.device)
#                 hidden = self.init_hidden()
#
#                 if self.condition_task_weight > 0:
#                     condition = local_conditions[decreasing_order].long().to(self.device)
#                     y_pred, pred_condition = self.forward(x, seq_lengths, hidden)
#                     loss = self.loss(y_pred, y, pred_condition, condition)
#                 else:
#                     y_pred = self.forward(x, seq_lengths, hidden)
#                     loss = self.loss(y_pred, y)
#
#                 loss.backward()
#                 optimizer.step()
#                 train_epoch_loss += loss.item()
#
#             dev_epoch_loss = 0.
#             # Setup the model in eval mode (does not compute gradients)
#             self.eval()
#             with torch.no_grad():
#                 for i, (local_batch, local_labels, seq_lengths, local_conditions) in enumerate(
#                         validation_generator):
#                     # order batch and send to device
#                     decreasing_order = (-seq_lengths).argsort()
#                     seq_lengths = seq_lengths[decreasing_order]
#                     batch_max_len = max(seq_lengths)
#                     x = local_batch[decreasing_order][:, :batch_max_len].long().to(self.device)
#                     y = local_labels[decreasing_order][:, :batch_max_len].long().to(self.device)
#                     hidden = self.init_hidden()
#
#                     if self.condition_task_weight > 0:
#                         condition = local_conditions[decreasing_order].long().to(self.device)
#                         y_pred, pred_condition = self.forward(x, seq_lengths, hidden)
#                         loss = self.loss(y_pred, y, pred_condition, condition)
#                     else:
#                         y_pred = self.forward(x, seq_lengths, hidden)
#                         loss = self.loss(y_pred, y)
#
#                     dev_epoch_loss += loss
#
#             train_total_loss += train_epoch_loss
#             dev_total_loss += dev_epoch_loss
#             train_loss_history.append(train_epoch_loss / len(train_ids))
#             validation_loss_history.append(dev_epoch_loss / len(validation_ids))
#             print(
#                 "\n Epoch {:.0f}, Train epoch Loss = {:.4f}, Dev epoch Loss = {:.4f}, time elapsed: {:.4f}".format(
#                     epoch,
#                     train_epoch_loss / len(train_ids),
#                     dev_epoch_loss / len(validation_ids),
#                     time.time() - epoch_start)
#             )
#             print("Total train Loss: {}".format(train_total_loss))
#
#         print("------------------------------------------------")
#         print(
#             "\n Training ended, \n Total train Loss = {:.4f}, Total dev Loss = {:.4f}, total time elapsed: {:.4f}".format(
#                 train_total_loss, dev_total_loss, time.time() - start_time))
#         return train_loss_history, validation_loss_history, epoch
#
#     def evaluate(self,
#                  sequences,
#                  patients_ids,
#                  at_k_number: int = 10,
#                  eval_device: str = 'cpu'):
#
#         dataloader_params = {
#             'batch_size': self.batch_size,
#             'shuffle': True,
#             'num_workers': self.dataloader_nbworkers,
#             'drop_last': True}
#         score_set = MedicalCodesDataset(sequences,
#                                         patients_ids,
#                                         self.max_len,
#                                         self.padding_idx,
#                                         condition_task_weight=self.condition_task_weight)
#         score_generator = data.DataLoader(score_set, **dataloader_params)
#
#         # not enough memory on gpu to keep the whole data set as tensors
#         eval_model = self.to(eval_device)
#
#         topks_golds = []
#         topks_preds = []
#         y_golds = []
#         condition_golds = []
#         condition_preds = []
#
#         eval_model.eval()
#         with torch.no_grad():
#             for i, (local_batch, local_labels, seq_lengths, local_conditions) in enumerate(
#                     score_generator):
#                 # order batch and send to device
#                 decreasing_order = (-seq_lengths).argsort()
#                 seq_lengths = seq_lengths[decreasing_order]
#                 batch_max_len = max(seq_lengths)
#                 x = local_batch[decreasing_order][:, :batch_max_len].long().to(eval_device)
#                 y = local_labels[decreasing_order][:, :batch_max_len].long().to(eval_device)
#                 hidden = eval_model.init_hidden()
#
#                 batch_gold_paddings = torch.ones(
#                     self.batch_size,
#                     self.max_len - batch_max_len) * self.padding_idx
#                 batch_pred_paddings = torch.ones(
#                     self.batch_size, self.max_len - batch_max_len,
#                     self.vocabulary_size)
#
#                 if eval_model.condition_task_weight > 0:
#                     batch_condition = local_conditions[decreasing_order].long()
#                     batch_y_pred, batch_condition_pred = eval_model.forward(x, seq_lengths, hidden)
#                     condition_golds.append(batch_condition.cpu())
#                     condition_preds.append(batch_condition_pred.cpu())
#
#                 else:
#                     batch_y_pred = eval_model.forward(x, seq_lengths, hidden)
#
#                 #  compute accuracy_at_k
#                 y_pred_padded = torch.cat(
#                     (batch_y_pred, batch_pred_paddings.float().to(eval_device)), dim=1)
#                 y_gold_padded = torch.cat((y, batch_gold_paddings.long().to(eval_device)), dim=1)
#                 batch_topks = y_pred_padded.topk(at_k_number, dim=2)[1]
#                 # @todo remove comment if working on bigoudi
#                 # workaround with numpy to avoid to use torch.repeat_interleave only available in torch 1.1
#                 batch_golds_repeated = torch.repeat_interleave(
#                     y_gold_padded, at_k_number, dim=1).view(
#                     y_gold_padded.size()[0], y_gold_padded.size()[1], -1)
#                 # batch_golds_repeated = np.resize(
#                 #     np.repeat(y_gold_padded.cpu().numpy(), at_k_number, axis=1),
#                 #     (y_gold_padded.size()[0], y_gold_padded.size()[1], at_k_number))
#                 # batch_golds_repeated = torch.from_numpy(batch_golds_repeated).long()
#                 topks_golds.append(batch_golds_repeated.cpu())
#                 topks_preds.append(batch_topks.cpu())
#                 y_golds.append(y_gold_padded.cpu())
#             topks_preds = torch.cat(topks_preds)
#             topks_golds = torch.cat(topks_golds)
#             y_golds = torch.cat(y_golds)
#             mask = (y_golds != self.padding_idx).long()
#             correctly_predicted = ((topks_preds == topks_golds).sum(2) * mask).sum().item()
#             nb_events = (torch.ones_like(y_golds) * mask).sum().item()
#             accuracy_at_k = {'accuracy@k': correctly_predicted / nb_events,
#                              'at_k_number': at_k_number}
#
#         if eval_model.condition_task_weight > 0:
#             return accuracy_at_k, \
#                    torch.cat(condition_golds), \
#                    torch.cat(condition_preds)
#         else:
#             return accuracy_at_k
