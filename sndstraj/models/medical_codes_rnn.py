import json

import numpy as np
import time as time
import torch
import torch.nn as nn
from torch.utils import data

from sndstraj.eventsTransformers.codesSequences import MedicalCodesDataset


class McRnn(nn.Module):
    def __init__(
            self,
            vocab,
            pad_token,
            device,
            max_len=10,
            batch_size=32,
            pretrained_embeddings_path: str = None,
            trainable_embeddings: bool = True,
            embedding_dim=100,
            hidden_dim=128,
            dropout=0.5,
            condition_task_weight=0.5,
            prediction_layer='mean_pooling',
            dataloader_nbworkers: int = 8,
            verbose: int = 1):

        super(McRnn, self).__init__()
        prediction_layer_possibilities = ['mean_pooling', 'max_pooling', 'last_hidden']
        assert prediction_layer in prediction_layer_possibilities, \
            'prediction_layer should be in {}'.format(prediction_layer_possibilities)

        # sep stands for Sequential Event Prediction (predict next events)
        assert (0 <= condition_task_weight) and (condition_task_weight <= 1), \
            print("Enter correct weight for the condition task weight: should be in [0, 1]")

        self.device = device
        self.vocab = vocab
        self.max_len = max_len
        self.vocabulary_size = len(vocab)
        self.padding_idx = self.vocab[pad_token]
        self.batch_size = batch_size
        self.pretrained_embeddings_path = pretrained_embeddings_path
        self.trainable_embeddings = trainable_embeddings
        self.embedding_dim = embedding_dim
        self.hidden_dim = hidden_dim
        self.dropout = dropout
        self.condition_task_weight = condition_task_weight
        self.prediction_layer = prediction_layer
        self.dataloader_nbworkers = dataloader_nbworkers

        self.embeddings = self.init_embeddings(verbose=verbose)

        # dropout layer for regularization
        self.drop = nn.Dropout(self.dropout)
        self.bn1 = nn.BatchNorm1d(self.hidden_dim)

        # recurrent unit layer (lstm because this is robust, but could try with GRU cells)
        self.recurrent = nn.LSTM(
            input_size=self.embedding_dim,
            hidden_size=self.hidden_dim,
            num_layers=1,
            batch_first=True)

        # map the hidden state to the vocabulary size to predict next word
        self.hidden2voca = nn.Linear(self.hidden_dim, self.vocabulary_size)
        self.sep_log_softmax = nn.LogSoftmax(dim=1)

        if self.condition_task_weight > 0:
            self.hidden2binary = nn.Linear(self.hidden_dim, 2)
            self.condition_log_softmax = nn.LogSoftmax(dim=1)

    def init_embeddings(self, verbose: int = 1):
        if self.pretrained_embeddings_path is not None:
            with open(self.pretrained_embeddings_path, 'r') as f:
                pretrained_embeddings_dict = json.load(f)
            pretrained_dim = len(list(pretrained_embeddings_dict.values())[0])
            assert pretrained_dim == self.embedding_dim, \
                'Pretrained Embeddings should be of the same dimension as model embedding dimension, catch {} but expected {}'.format(
                    pretrained_dim, self.embedding_dim)
            weights_matrix = np.zeros((len(self.vocab), self.embedding_dim))
            nb_replaced_embeddings = 0
            for code, ix in self.vocab.items():
                if code in pretrained_embeddings_dict.keys():
                    weights_matrix[ix] = pretrained_embeddings_dict[code]
                    nb_replaced_embeddings += 1
                else:
                    weights_matrix[ix] = np.random.normal(scale=0.6, size=(self.embedding_dim,))
            # emb_layer = nn.Embedding(self.vocabulary_size, self.embedding_dim)
            embeddings = nn.Embedding.from_pretrained(torch.Tensor(weights_matrix))
            if verbose:
                print('{}/{} codes in voca match pretrained embeddings'.format(
                    nb_replaced_embeddings, len(self.vocab)))
        else:
            embeddings = nn.Embedding(
                num_embeddings=self.vocabulary_size,
                embedding_dim=self.embedding_dim,
                padding_idx=self.padding_idx)
        # trainable or frozen embeddings ?
        embeddings.weight.requires_grad = self.trainable_embeddings
        if verbose:
            print("Trainable embeddings : {}".format(self.trainable_embeddings))
        return embeddings

    def init_hidden(self):
        # Before we've done anything, we don't have any hidden state.
        # The axes semantics are (num_layers, minibatch_size, hidden_dim)
        weight = next(self.parameters())
        return (
            weight.new_zeros(1, self.batch_size, self.hidden_dim),
            weight.new_zeros(1, self.batch_size, self.hidden_dim))

    def forward(self, batch, batch_lengths, hidden):
        batch_size, seq_len = batch.size()
        # embedding : (batch_size, seq_len, 1) -> (batch_size, seq_len, embedding_dim)
        embeds = self.embeddings(batch)
        embeds_dropped = self.drop(embeds)
        # pack_padded_sequence to hide padded items to the recurrent layer
        packed = nn.utils.rnn.pack_padded_sequence(
            embeds_dropped,
            lengths=batch_lengths,
            batch_first=True)
        rec_out, hidden = self.recurrent(packed, hidden)
        rec_out_padded, _ = nn.utils.rnn.pad_packed_sequence(rec_out, batch_first=True)

        # (batch_size, seq_len, nb_hidden) -> (batch_size * seq_len, nb_hidden) for log softmax
        rec_out_padded = rec_out_padded.contiguous()
        rec_out_resized = rec_out_padded.view(-1, self.hidden_dim)
        out = self.hidden2voca(rec_out_resized)

        # log-softmax
        log_probs = self.sep_log_softmax(out)
        # consider using adaptive soft max for imbalance distribution over outputs:
        # https://pytorch.org/docs/stable/nn.html?highlight=logsoftmax#torch.nn.AdaptiveLogSoftmaxWithLoss

        if self.condition_task_weight > 0:
            # crush temporal dimension :
            # (batch_size, seq_len, hidden_dim) -> (batch_size, hidden_dim)
            context_vector = torch.zeros(self.hidden_dim)
            if self.prediction_layer == 'last_hidden':
                context_vector = hidden[0].squeeze()
            elif self.prediction_layer == 'max_pooling':
                #  max_pool on outputs along temporal dimension
                context_vector = rec_out_padded.max(dim=1)[0]
            elif self.prediction_layer == 'mean_pooling':
                #  mean_pool on outputs along temporal dimension
                context_vector = (rec_out_padded.sum(dim=1) / seq_len).squeeze()
            # binary prediction: (batch_size, hidden_dim) -> (batch_size, 2)
            # context_vector_dropped = self.drop(context_vector)
            context_vector_bn = self.bn1(context_vector)
            condition = self.condition_log_softmax(self.hidden2binary(context_vector_bn))

            #  (batch_size * seq_len, nb_hidden) -> (batch_size, seq_len, vocabulary_size)
            return log_probs.view(batch_size, seq_len, self.vocabulary_size), condition
        else:
            #  (batch_size * seq_len, nb_hidden) -> (batch_size, seq_len, vocabulary_size)
            return log_probs.view(batch_size, seq_len, self.vocabulary_size)

    def loss(self, y_pred, y, pred_condition=None, condition=None):
        """A loss function that deals with padded items and multi-task learning"""

        y = y.contiguous().view(-1)
        # flatten all predictions
        y_pred = y_pred.contiguous().view(-1, self.vocabulary_size)
        # create a mask by filtering out all tokens that ARE NOT the padding token
        tag_pad_token = self.padding_idx
        mask = (y != tag_pad_token).float()
        # count how many tokens we have
        nb_tokens = int(torch.sum(mask).item())

        # weighted Cross-entropy loss with the mask  as weights (ignore <PAD> tokens)
        y_pred = y_pred[range(y_pred.shape[0]), y] * mask
        ce_loss = -torch.sum(y_pred) / nb_tokens

        # cross entropy loss for condition
        condition_loss = nn.NLLLoss()
        #  choose supervision depending on the task
        if self.condition_task_weight > 0:
            condition = condition.contiguous().view(-1)
            pred_condition = pred_condition.contiguous().view(-1, 2)
            return (1 - self.condition_task_weight) * ce_loss + self.condition_task_weight * condition_loss(pred_condition, condition)
        elif self.condition_task_weight == 1:
            condition = condition.contiguous().view(-1)
            pred_condition = pred_condition.contiguous().view(-1, 2)
            return condition_loss(pred_condition, condition)
        elif self.condition_task_weight == 0:
            return ce_loss

    def train_model(
            self,
            sequences,
            train_ids,
            validation_ids,
            optimizer,
            nb_epochs
    ):

        dataloader_params = {
            'batch_size': self.batch_size,
            'shuffle': True,
            'num_workers': self.dataloader_nbworkers,
            'drop_last': True
        }

        # optimizer = optim.SGD(model.parameters(), lr=0.1)

        #  Build data generators
        train_set = MedicalCodesDataset(sequences,
                                        train_ids,
                                        self.max_len,
                                        self.padding_idx,
                                        condition_task_weight=self.condition_task_weight)
        train_generator = data.DataLoader(train_set, **dataloader_params)

        validation_set = MedicalCodesDataset(sequences,
                                             validation_ids,
                                             self.max_len,
                                             self.padding_idx,
                                             condition_task_weight=self.condition_task_weight)
        validation_generator = data.DataLoader(validation_set, **dataloader_params)

        # initialise history
        train_loss_history = []
        validation_loss_history = []
        train_total_loss = 0.
        dev_total_loss = 0.

        self.train()
        start_time = time.time()

        # TODO: let epoch be a parameter of the network
        epoch = 0
        for epoch in range(nb_epochs):
            epoch_start = time.time()
            train_epoch_loss = 0.
            # setup the model in train mode
            self.train()
            for i, (local_batch, local_labels, seq_lengths, local_conditions) in enumerate(
                    train_generator):
                optimizer.zero_grad()
                self.zero_grad()
                # order batch and send to device
                decreasing_order = (-seq_lengths).argsort()
                seq_lengths = seq_lengths[decreasing_order]
                batch_max_len = max(seq_lengths)
                x = local_batch[decreasing_order][:, :batch_max_len].long().to(self.device)
                y = local_labels[decreasing_order][:, :batch_max_len].long().to(self.device)
                hidden = self.init_hidden()

                if self.condition_task_weight > 0:
                    condition = local_conditions[decreasing_order].long().to(self.device)
                    y_pred, pred_condition = self.forward(x, seq_lengths, hidden)
                    loss = self.loss(y_pred, y, pred_condition, condition)
                else:
                    y_pred = self.forward(x, seq_lengths, hidden)
                    loss = self.loss(y_pred, y)

                loss.backward()
                optimizer.step()
                train_epoch_loss += loss.item()

            dev_epoch_loss = 0.
            # Setup the model in eval mode (does not compute gradients)
            self.eval()
            with torch.no_grad():
                for i, (local_batch, local_labels, seq_lengths, local_conditions) in enumerate(
                        validation_generator):
                    # order batch and send to device
                    decreasing_order = (-seq_lengths).argsort()
                    seq_lengths = seq_lengths[decreasing_order]
                    batch_max_len = max(seq_lengths)
                    x = local_batch[decreasing_order][:, :batch_max_len].long().to(self.device)
                    y = local_labels[decreasing_order][:, :batch_max_len].long().to(self.device)
                    hidden = self.init_hidden()

                    if self.condition_task_weight > 0:
                        condition = local_conditions[decreasing_order].long().to(self.device)
                        y_pred, pred_condition = self.forward(x, seq_lengths, hidden)
                        loss = self.loss(y_pred, y, pred_condition, condition)
                    else:
                        y_pred = self.forward(x, seq_lengths, hidden)
                        loss = self.loss(y_pred, y)

                    dev_epoch_loss += loss

            train_total_loss += train_epoch_loss
            dev_total_loss += dev_epoch_loss
            train_loss_history.append(train_epoch_loss / len(train_ids))
            validation_loss_history.append(dev_epoch_loss / len(validation_ids))
            print(
                "\n Epoch {:.0f}, Train epoch Loss = {:.4f}, Dev epoch Loss = {:.4f}, time elapsed: {:.4f}".format(
                    epoch,
                    train_epoch_loss / len(train_ids),
                    dev_epoch_loss / len(validation_ids),
                    time.time() - epoch_start)
            )
            print("Total train Loss: {}".format(train_total_loss))

        print("------------------------------------------------")
        print(
            "\n Training ended, \n Total train Loss = {:.4f}, Total dev Loss = {:.4f}, total time elapsed: {:.4f}".format(
                train_total_loss, dev_total_loss, time.time() - start_time))
        return train_loss_history, validation_loss_history, epoch

    def evaluate(self,
                 sequences,
                 patients_ids,
                 at_k_number: int = 10,
                 eval_device: str = 'cpu'):

        dataloader_params = {
            'batch_size': self.batch_size,
            'shuffle': True,
            'num_workers': self.dataloader_nbworkers,
            'drop_last': True}
        score_set = MedicalCodesDataset(sequences,
                                        patients_ids,
                                        self.max_len,
                                        self.padding_idx,
                                        condition_task_weight=self.condition_task_weight)
        score_generator = data.DataLoader(score_set, **dataloader_params)

        # not enough memory on gpu to keep the whole data set as tensors
        eval_model = self.to(eval_device)

        topks_golds = []
        topks_preds = []
        y_golds = []
        condition_golds = []
        condition_preds = []

        eval_model.eval()
        with torch.no_grad():
            for i, (local_batch, local_labels, seq_lengths, local_conditions) in enumerate(
                    score_generator):
                # order batch and send to device
                decreasing_order = (-seq_lengths).argsort()
                seq_lengths = seq_lengths[decreasing_order]
                batch_max_len = max(seq_lengths)
                x = local_batch[decreasing_order][:, :batch_max_len].long().to(eval_device)
                y = local_labels[decreasing_order][:, :batch_max_len].long().to(eval_device)
                hidden = eval_model.init_hidden()

                batch_gold_paddings = torch.ones(
                    self.batch_size,
                    self.max_len - batch_max_len) * self.padding_idx
                batch_pred_paddings = torch.ones(
                    self.batch_size, self.max_len - batch_max_len,
                    self.vocabulary_size)

                if eval_model.condition_task_weight > 0:
                    batch_condition = local_conditions[decreasing_order].long()
                    batch_y_pred, batch_condition_pred = eval_model.forward(x, seq_lengths, hidden)
                    condition_golds.append(batch_condition.cpu())
                    condition_preds.append(batch_condition_pred.cpu())

                else:
                    batch_y_pred = eval_model.forward(x, seq_lengths, hidden)

                #  compute accuracy_at_k
                y_pred_padded = torch.cat(
                    (batch_y_pred, batch_pred_paddings.float().to(eval_device)), dim=1)
                y_gold_padded = torch.cat((y, batch_gold_paddings.long().to(eval_device)), dim=1)
                batch_topks = y_pred_padded.topk(at_k_number, dim=2)[1]
                #@todo remove comment if working on bigoudi
                # workaround with numpy to avoid to use torch.repeat_interleave only available in torch 1.1
                batch_golds_repeated = torch.repeat_interleave(
                    y_gold_padded, at_k_number, dim=1).view(
                    y_gold_padded.size()[0], y_gold_padded.size()[1], -1)
                # batch_golds_repeated = np.resize(
                #     np.repeat(y_gold_padded.cpu().numpy(), at_k_number, axis=1),
                #     (y_gold_padded.size()[0], y_gold_padded.size()[1], at_k_number))
                # batch_golds_repeated = torch.from_numpy(batch_golds_repeated).long()
                topks_golds.append(batch_golds_repeated.cpu())
                topks_preds.append(batch_topks.cpu())
                y_golds.append(y_gold_padded.cpu())
            topks_preds = torch.cat(topks_preds)
            topks_golds = torch.cat(topks_golds)
            y_golds = torch.cat(y_golds)
            mask = (y_golds != self.padding_idx).long()
            correctly_predicted = ((topks_preds == topks_golds).sum(2) * mask).sum().item()
            nb_events = (torch.ones_like(y_golds) * mask).sum().item()
            accuracy_at_k = {'accuracy@k': correctly_predicted / nb_events,
                             'at_k_number': at_k_number}

        if eval_model.condition_task_weight > 0:
            return accuracy_at_k, \
                   torch.cat(condition_golds), \
                   torch.cat(condition_preds)
        else:
            return accuracy_at_k
