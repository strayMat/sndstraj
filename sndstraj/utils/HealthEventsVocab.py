import json
from typing import List, Dict

import numpy as np
import pyspark.sql.functions as func
from pyspark.sql import DataFrame

SOS = "<sos>"
EOS = "<eos>"
PAD = "<pad>"
MASK = "<mask>"


class HealthEventsVocab(object):
    """
    Create a vocabulary from events dataframe (containing at least a `value` column)
    with two sub vocabularies :
    - values for medical concepts
    - time deltas for timing between events
    """
    def __init__(
            self,
            categories_blacklist: List[str] = None,
            sos: str = SOS,
            eos: str = EOS,
            pad: str = PAD,
            mask: str = MASK):
        if categories_blacklist is None:
            self.categories_blacklist = []
        else:
            self.categories_blacklist = categories_blacklist
        self.value2ix, self.ix2value = (None, None)
        self.delta2ix, self.ix2delta = (None, None)

        self.pad_token = pad
        self.mask_token = mask
        self.sos_token = sos
        self.eos_token = eos

        self.pad_ix = 0
        self.mask_ix = 1
        self.sos_ix = 2
        self.eos_ix = 3
        return

    def build_vocab_from_events(
            self,
            events: DataFrame,
            with_deltas: bool = True,
            verbose: int = 1):
        """
        Build a vocabulary from an events table.
        :param events:
        :param with_deltas:
        :param verbose:
        :return:
        """
        events = events.filter(~func.col("category").isin(self.categories_blacklist))

        values_items = [v for v in np.array(events.select(
            func.col("value")).distinct().collect()).flatten()]
        self.value2ix, self.ix2value = get_label_ix_dicts(
            [self.pad_token, self.mask_token, self.sos_token, self.eos_token] + values_items
        )

        if with_deltas:
            deltas_items = [int(v) for v in np.array(events.select(
                func.col("delta")).distinct().collect()).flatten()]
            self.delta2ix, self.ix2delta = get_label_ix_dicts(
                [self.pad_token, self.mask_token, self.sos_token, self.eos_token] + deltas_items
            )

        if verbose:
            print("Value vocabulary size : {}".format(len(self.value2ix)))
            if with_deltas:
                print("Delta vocabulary size : {}".format(len(self.delta2ix)))
        return

    def save_to_json(self, path2vocab: str):
        vocab_dic = {
            'value2ix': self.value2ix, 'ix2value': self.ix2value,
            'delta2ix': self.delta2ix, 'ix2delta': self.ix2delta
        }

        with open(path2vocab, 'w') as f:
            json.dump(vocab_dic, f)
        return

    def load_from_json(self, path2vocab: str):
        with open(path2vocab, 'r') as f:
            vocab_dic = json.load(f)
        self.value2ix = vocab_dic['value2ix']
        self.ix2value = vocab_dic['ix2value']
        self.delta2ix = vocab_dic['delta2ix']
        self.ix2delta = vocab_dic['ix2delta']
        return


def vocab_from_json(path2vocab: str) -> HealthEventsVocab:
    """
    Create a vocabulary from a json file.
    :param path2vocab:
    :return:
    """
    vocab = HealthEventsVocab()
    vocab.load_from_json(path2vocab)
    return vocab


def get_label_ix_dicts(vocabulary_terms: List) -> (Dict, Dict):
    """
    Create label2ix and ix2label dictionaries from a collection of unique terms extracted from a
    corpus.
    :param vocabulary_terms:
    :return:
    """
    label2ix = {}
    for c in vocabulary_terms:
        if c not in label2ix:
            label2ix[c] = len(label2ix)
    ix2label = {}
    for c, i in label2ix.items():
        ix2label[str(i)] = c
    return label2ix, ix2label
