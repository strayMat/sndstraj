import json
import os
from functools import reduce
from typing import Tuple, Dict

import pandas as pd
from pyspark import SparkConf
from pyspark.sql import SQLContext, DataFrame
from pyspark.sql import SparkSession


def start_sc(
        nb_threads: int = 4,
        arrow_enabled: bool = False):
    conf = SparkConf().setAll([
        ('spark.master', 'local[{}]'.format(nb_threads)),
        ('spark.executor.memory', '4g'),
        ('spark.driver.memory', '4g'),
        ('spark.sql.shuffle.partitions', '500'),
        ('spark.default.parallelism', '500'),
        ('spark.driver.maxResultSize', '20g'),
        ("spark.sql.execution.arrow.enabled", str(arrow_enabled).lower()),
        ("spark.app.name", "sndstraj")
    ])
    sc = SparkSession.builder.config(conf=conf).getOrCreate()
    return sc


def start_bigoudi_sc(
        nb_executors: int = 4,
        local: bool = False,
        arrow_enabled: bool = False,
        executor_memory: str = '32g',
        driver_memory: str = '32g',
        max_result_size: str = '64g',
        parallelism: int = 500,
        conf_only: bool = False,
        app_name: str = "sndstraj"):
    """
    # taking inspiration from : https://spoddutur.github.io/spark-notes/distribution_of_executors_cores_and_memory_for_spark_application.html
    bigoudi is 1 node, 80 cores, 2000Go Ram
    Logic:
        - fix 4 executors per exec,
        - fix 32GB per exec by default,
        - let the user control the number of executors with a 20 limit (20*4=80 cores, 32**2090 heap memory=669 GB)
    :param app_name:
    :param conf_only:
    :param nb_executors:
    :param local:
    :param arrow_enabled:
    :param executor_memory:
    :param driver_memory:
    :param max_result_size: used for spark actions (eg. collect) or for the driver heap allocation (if you have huged execution graphs)
    :param parallelism:
    :return:
    """

    if local:
        locality = [('spark.master', 'local[{}]'.format(nb_executors * 4))]
    else:
        locality = [
            ('spark.master', 'spark://localhost:7077'),
            ('spark.executor.cores', '4'),
            ('spark.num.executors', '{}'.format(nb_executors))
        ]
    conf = SparkConf().setAll([
        *locality,
        ('spark.executor.memory', '{}'.format(executor_memory)),
        ('spark.driver.memory', '{}'.format(driver_memory)),
        ('spark.cores.max', '{}'.format(4 * nb_executors)),
        ('spark.sql.shuffle.partitions', '{}'.format(parallelism)),
        ('spark.default.parallelism', '{}'.format(parallelism)),
        ('spark.driver.maxResultSize', '{}'.format(max_result_size)),
        ("spark.sql.execution.arrow.enabled", str(arrow_enabled).lower()),
        ('spark.driver.extraJavaOptions',
         '-Djava.io.tmpdir=/home/commun/echange/matthieu_doutreligne/tmp/'),
        ('spark.executor.extraJavaOptions',
         '-Djava.io.tmpdir=/home/commun/echange/matthieu_doutreligne/tmp/'),
        ('spark.dynamicAllocation.enabled', 'true'),
        ('spark.shuffle.service.enabled', 'true'),
        ("spark.app.name", app_name)
    ])
    if conf_only:
        return conf
    sc = SparkSession.builder.config(conf=conf).getOrCreate()
    return sc


def load_data(
        sqlContext: SQLContext,
        path2metadata: str) -> Tuple[Dict, Dict]:
    """
    Description: Load data from a featuring metadata file pointing to the files paths
    :param sqlContext: pyspark.SQLContext
    :param path2metadata:
    :return: two dictionaries containing respectively events and patients data
    """
    with open(path2metadata, 'r') as f:
        metadata = json.load(f)

    events = {}
    patients = {}
    for operation in metadata['operations']:
        operation_name = operation['name']
        if operation['population_path'] != '':
            events[operation_name] = sqlContext.read.parquet(operation['output_path'])
            patients[operation_name] = sqlContext.read.parquet(operation['population_path'])
        else:
            events[operation_name] = sqlContext.read.parquet(operation['output_path'])

    return events, patients


def train_dev_test_split(
        path2split: str,
        path2sequences: str,
        ratios=(0.6, 0.2, 0.2)):
    """
    Description: After the definition of the population,this code should be launched once at the beginning of
    the study to build once and for all the train, valid and test sets.
    :param path2split:
    :param path2sequences:
    :param ratios:
    :return:
    """

    sequences = pd.read_csv(path2sequences, index_col="patientID").sample(
        frac=1)
    patient_ids = sequences.index.values

    # Datasets
    train_prop, valid_prop, test_prop = ratios
    nb_patients = len(patient_ids)

    train_df = pd.DataFrame({'patient_id': patient_ids[:int(nb_patients * train_prop)]})
    valid_df = pd.DataFrame({'patient_id': patient_ids[int(nb_patients * train_prop):int(
        nb_patients * (train_prop + valid_prop))]})
    test_df = pd.DataFrame(
        {'patient_id': patient_ids[int(nb_patients * (train_prop + valid_prop)):]})

    train_df.to_csv(os.path.join(path2split, 'patients_train_ids.csv'), index=False)
    valid_df.to_csv(os.path.join(path2split, 'patients_valid_ids.csv'), index=False)
    test_df.to_csv(os.path.join(path2split, 'patients_test_ids.csv'), index=False)

    return


def union_all(*dfs: DataFrame):
    return reduce(DataFrame.union, dfs)
