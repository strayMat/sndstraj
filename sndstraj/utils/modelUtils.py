import os
import re
from datetime import datetime
from typing import Tuple, Dict

# from deprecated import deprecated

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import torch
from torch import save


# this should be stabilized for a given model
# (in fact, the save_model and load_model should belong to one given model (not common)
def save_model(
        model,
        model_args,
        partition,
        optimizer,
        evaluation,
        epoch,
        train_loss_history,
        dev_loss_history,
        results_dir,
        results_name='0') -> None:
    """
    Save model as recommanded in
    https://pytorch.org/tutorials/beginner/saving_loading_models.html#saving-loading-model-for-inference
    To load the model use:
        checkpoint = torch.load(PATH)
        model_args =  checkpoint['model_args']
        model = TheModelClass(**model_args)
        partition = checkpoint['partition']
        optimizer = TheOptimizerClass(*args, **kwargs)
        model.load_state_dict(checkpoint['model_state_dict'])
        optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        epoch = checkpoint['epoch']
        train_loss_history = checkpoint['train_loss_history']
        dev_loss_history = checkpoint['dev_loss_history']
        evaluation= checkpoint['score']
    """

    if not os.path.isdir(results_dir):
        os.mkdir(results_dir)
    save_time = datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
    exp_path = os.path.join(results_dir, "exp_{}_{}.pt".format(save_time, results_name))
    save({
        'model_state_dict': model.state_dict(),
        'partition': partition,
        'optimizer_state_dict': optimizer.state_dict(),
        'model_args': model_args,
        'evaluation': evaluation,
        'epoch': epoch,
        'train_loss_history': train_loss_history,
        'dev_loss_history': dev_loss_history
    }, exp_path)

    print("Saving experience at {}".format(exp_path))


def plot_history(
        train_history,
        dev_history,
        save_figure=None) -> None:
    plt.style.use("dark_background")
    plt.plot(np.arange(len(train_history)), train_history, color="#D6280B", label="Train Loss")
    plt.plot(np.arange(len(dev_history)), dev_history, color="#27FF4D", label="Test Loss")
    plt.legend()
    plt.show()
    if save_figure is not None:
        plt.savefig()


# @deprecated("This function was included in the model, please use `medical_code_rnn.model.eval` instead")
def accuracy_at_k(
        y_predicted,
        y_golds,
        pad_ix,
        at_k_number=10) -> Dict[str, float]:
    topks = y_predicted.topk(at_k_number, dim=2)[1]
    # workaround with numpy to avoid to use torch.repeat_interleave only available in torch 1.1
    golds = torch.repeat_interleave(
        y_golds, at_k_number, dim=1).view(y_golds.size()[0], y_golds.size()[1], -1)
    # golds = np.resize(
    #     np.repeat(y_golds.numpy(), at_k_number, axis=1),
    #     (y_golds.size()[0], y_golds.size()[1], at_k_number))
    # golds = torch.from_numpy(golds).long()
    mask = (y_golds != pad_ix).long()
    correctly_predicted = ((topks == golds).sum(2) * mask).sum().item()
    nb_events = (torch.ones_like(y_golds) * mask).sum().item()

    return {'accuracy@k': correctly_predicted / nb_events, 'at_k_number': at_k_number}


def confusion(predictions: np.ndarray, golds: np.ndarray) -> Tuple[int, int, int, int]:
    # predictions = predictions.argmax(dim=1)
    ones = np.ones_like(predictions)
    zeros = np.zeros_like(predictions)
    # element wise division of the two tensors:
    # 1 where predictions and golds are 1 (TP)
    # inf where predictions is 1 and golds is 0 (FP)
    # nan where predictions and golds are 0 (TN)
    # 0 where predictions is 0 and golds is 1 (FN)
    true_positives = int(np.sum((predictions == golds) * (predictions == ones)))
    false_positives = int(np.sum((predictions != golds) * (predictions == ones)))
    true_negatives = int(np.sum((predictions == golds) * (predictions == zeros)))
    false_negatives = int(np.sum((predictions != golds) * (predictions == zeros)))
    return true_positives, false_positives, true_negatives, false_negatives


def precision(tp: int, fp: int) -> float:
    if (tp != 0) | (fp != 0):
        return tp / (tp + fp)
    else:
        print('tp or fp are both 0')
        return 0


def recall(tp: int, fn: int) -> float:
    if (tp != 0) | (fn != 0):
        return tp / (tp + fn)
    else:
        print('tp and fn are both 0')
        return 0


def F1(tp: int, fp: int, fn: int) -> float:
    p = precision(tp, fp)
    r = recall(tp, fn)
    if (p != 0) | (r != 0):
        return 2 * r * p / (r + p)
    else:
        return 0


def condition_score(predictions: np.ndarray, golds: np.ndarray) -> Dict[str, float]:
    true_positives, false_positives, true_negatives, false_negatives = confusion(predictions, golds)
    return {
        'precision': precision(true_positives, false_positives),
        'recall': recall(true_positives, false_negatives),
        'F1': F1(true_positives, false_positives, false_negatives)
    }


def load_models_scores(models_path: str) -> pd.DataFrame:
    models_individual_paths = [f for f in os.listdir(models_path) if
                               re.search('.pt$', f) is not None]
    models_results = pd.DataFrame()
    for xp_name in models_individual_paths:
        checkpoint = torch.load(os.path.join(models_path, xp_name))
        scores = checkpoint['evaluation']
        condition_scores = scores['condition_score']
        model_args = checkpoint['model_args']
        model_args.pop('vocab', None).pop('pad_token', None)
        try:
            model_result = {
                'xp_name': xp_name,
                'accuracy@k': scores['sep_score']['accuracy@k'],
                'at_k_number': scores['sep_score']['at_k_number'],
                'F1': condition_scores['F1'],
                'precision': condition_scores['precision'],
                'recall': condition_scores['recall'],
                **model_args
            }
        except TypeError:
            print('Error loading {}'.format(xp_name))
        models_results = models_results.append(model_result, ignore_index=True)

    print('Loaded result from {} models'.format(models_results.shape[0]))
    return models_results
