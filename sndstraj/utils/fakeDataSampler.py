import random
import time

import numpy as np
import pandas as pd
import datetime as dt
from typing import Dict


DIAGNOSES_CODES = [
    "J440",  # Maladie pulmonaire obstructive avec infection aigue des VR inférieures
    "J441",  # Maladie pulmonaire obstructive avec épisode aigue sans précision
    "J448",  # Autres maladies pulmonaires obstructives précisées
    "J449",  # Maladie pulmonaire obstructive sans précision
    "J960",  # Insuffisance respi aigue (à chercher avec DA = J44*)
    "J181"  # Pneumopathie lombaire (à chercher avec DA = J44*)
    "J44"
]

DRUG_CODES = ["3400949500178", "3400949500697", "3400949502479", "3400949951321", "3400949974504",
              "3400949974672", "3400949974733", "3400949975105",  # "Antibiotiques"
              "3400933457396", "3400933457686", "3400933457808", "3400933459000", "3400933459468",
              "3400933459697", "3400934060335", "3400934060625", "3400934082276", "3400934082337",
              "3400934653896",  # "Bronchodilatateurs_Adrenergiques_anticolinergiques ou B2"
              "3400933468453",
              "3400933468743", "3400933468804", "3400933469115", "3400933469283", "3400933469573",
              "3400933469634", "3400933470005",  # substituts nicotiniques
              "3400933607852", "3400933652166", "3400935493828", "3400935947383", "3400939886091",
              "3400939886152", "3400939925745"  # "Vaccin_Anti_Grippal"
              ]

ACTS_CODES = [
    "GLQP012",
    # mesure de la capacité vitale lente et de l’expiration forcée avec enregistrement (spirométrie standard)
    "GLQP008",
    # mesure de la capacité vitale lente et de l’expiration forcée avec gazométrie sanguine artérielle (spirométrie standard avec gaz du sang)
    "GLQP002",
    # mesure de la capacité vitale lente et de l’expiration forcée avec mesure des volumes pulmonaires mobilisables et non mobilisables par pléthysismographie
    "GLQP009",
    # mesure de la capacité vitale lente et du volume courant par pléthysismographie d’inductance
    "GLQP003",  # mesure de l’expiration forcée
    "GLQP014",  # mesure du débit expiratoire maximal par technique de compression
    "GLQP011"
    # mesure des volumes pulmonaires mobilisables et non mobilisables par pléthysismographie
]

EVENT_CATEGORIES = {
    "main_diagnostic": DIAGNOSES_CODES,
    "drug": DRUG_CODES,
    "dcir_act": ACTS_CODES
}

PFS_ids = ["pfsID-{}".format(i) for i in np.random.randint(1, 100, 100)]

START_DATE = "2017-01-01"
END_DATE = "2018-01-01"

WEIGHTS = 0.0

ENDS = pd.Timestamp(None)

EVENT_DF_COLUMNS = ["patientID", "category", "value", "groupID", "weight", "start", "end"]


def sample_trajectories(
        nb_patients: int = 1000,
        poisson_lambda: float = 10,
        event_categories: Dict = None) -> pd.DataFrame:
    '''
    Description: Simulate a cohort events in the form of event dataframe
    :param nb_patients: number of patients (default=1000)
    :param poisson_lambda: lambda parameter for the poisson law modeling the number of events by patient (default=10)
    :param event_categories: default event categories

    :return: event dataframe of the form ["patientID", "category", "value", "groupID", "weight", "start", "end"]
    '''
    if event_categories is None:
        event_categories = EVENT_CATEGORIES
    nb_events = np.random.poisson(poisson_lambda, nb_patients)
    patients_list = [sample_patient_traj(i, nb_events[i], event_categories=event_categories) for i in np.arange((nb_patients))]
    event_df = pd.concat(patients_list)

    event_df['start'] = pd.to_datetime(event_df['start'])
    event_df['end'] = pd.to_datetime(event_df['end'])

    return event_df


def sample_patient_traj(
        patient_id,
        nb_events,
        event_categories: Dict = None) -> pd.DataFrame:

    if event_categories is None:
        event_categories = EVENT_CATEGORIES
    categories = np.random.choice(list(event_categories.keys()), nb_events, True)
    pat_id = "pID-{}".format(patient_id)
    events = [np.random.choice(event_categories[event]) for event in categories]
    starts = [random_date(START_DATE, END_DATE, random.random()) for i in np.arange(nb_events)]
    patient_list = [np.repeat(pat_id, nb_events),
                    categories,
                    events,
                    np.random.choice(PFS_ids, nb_events, True),
                    np.repeat(WEIGHTS, nb_events),
                    starts,
                    np.repeat(ENDS, nb_events)]

    patient_df = pd.DataFrame(np.transpose(patient_list), columns=EVENT_DF_COLUMNS)

    return patient_df


def sample_patients(
        nb_patients: int) -> pd.DataFrame:
    """
    Description: Simulate a cohort subjects with patient demographics.
    :param nb_patients: Number of patients to be simulated
    :return: Simulated patient demographics ('patientID', 'gender', 'birthDate', 'deathDate')
    """
    # hyper_parameters for sampling a population
    sex_ratio = 0.5
    life_expectancy = 80

    demographic_columns = ['patientID', 'gender', 'birthDate', 'deathDate']
    patient_ids = ['pID-{}'.format(i) for i in range(nb_patients)]
    genders = np.random.binomial(n=1, p=sex_ratio, size=nb_patients) + 1
    birth_dates = []
    for pat in range(nb_patients):
        birth_dates.append(
            dt.datetime.strptime(
                random_date(
                    start='1910-01-01',
                    end='2019-01-01',
                    prop=np.random.rand(1)),
                '%Y-%m-%d'))
    reference_date = dt.datetime.strptime('2019-09-21', '%Y-%m-%d')
    life_spans = []
    for life_span in np.random.normal(loc=life_expectancy, scale=10, size=nb_patients):
        life_spans.append(
            dt.timedelta(days=int(life_span*365)))
    raw_death_dates = [birth + life_span for (birth,life_span) in zip (birth_dates, life_spans)]
    death_dates = [d if d <= reference_date else None for d in raw_death_dates]
    patients_demographics = pd.DataFrame(
        data=list(zip(patient_ids, genders, birth_dates, death_dates)),
        columns=demographic_columns)
    return patients_demographics


def str_time_prop(start, end, format, prop):
    """
    Description: Get a time at a proportion of a range of two formatted times.
    start and end should be strings specifying times formated in the
    given format (strftime-style), giving an interval [start, end].
    prop specifies how a proportion of the interval to be taken after
    start.  The returned time will be in the specified format.
    """

    stime = time.mktime(time.strptime(start, format))
    etime = time.mktime(time.strptime(end, format))

    ptime = stime + prop * (etime - stime)

    return time.strftime(format, time.localtime(ptime))


def random_date(start, end, prop):
    return str_time_prop(start, end, '%Y-%m-%d', prop)
