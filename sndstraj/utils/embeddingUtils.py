from copy import deepcopy
from typing import List

import pandas as pd
from sklearn.metrics.pairwise import cosine_similarity


def concept_proximity(
        query_code: str,
        embeddings_df: pd.DataFrame,
        k: int = 1,
        target_terminologies: List[str] = None,
        keep_query: bool = False) -> pd.DataFrame:
    """
    Description: Compute the cosine distance between a query concepts and the other concepts,
     sort the results by decreasing distance.
    :param keep_query:
    :param query_code:
    :param embeddings_df:
    :param k:
    :param target_terminologies:
    :return:
    """

    if target_terminologies is None:
        target_terminologies = ['']
    # available_terminologies = ['ccam', 'cim10', 'nabm', 'atc', '']
    assert query_code in embeddings_df[
        'concept_code'].values, 'query_code should be in the embeddings dataframe'

    query_vector = embeddings_df.loc[
        embeddings_df['concept_code'] == query_code, 'concept_embedding'].values[0]

    concept_distances = deepcopy(embeddings_df)
    concept_distances['distance2query'] = cosine_similarity(
        [query_vector],
        list(concept_distances['concept_embedding'].values))[0]

    concept_distances.sort_values('distance2query', ascending=False, inplace=True)
    if not keep_query:
        concept_distances = concept_distances.loc[concept_distances['concept_code'] != query_code, :]
    else:
        k = k + 1

    if target_terminologies != ['']:
        top_concepts = concept_distances.loc[
                       concept_distances['concept_terminology'].isin(target_terminologies), :][:k]
    else:
        top_concepts = concept_distances[:k]

    kept_cols = ['concept_code', 'concept_name', 'concept_terminology', 'distance2query']
    top_concepts_cleaned = top_concepts.loc[:, kept_cols]

    return top_concepts_cleaned
